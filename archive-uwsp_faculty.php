<?php
/**
 * The template for displaying faculty archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package getwid_base
 */
get_header();

get_template_part('template-parts/faculty-archive');

get_footer();
