<?php
/*
for more information on taxonomies, go here:
http://codex.wordpress.org/Function_Reference/register_taxonomy
*/
flush_rewrite_rules();
// let's create the function for the custom type Expert
function custom_post_faculty() {
    // creating (registering) the custom type
    register_post_type( 'uwsp_faculty', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        // let's now add all the options for this post type
        array( 'labels' => array(
            'name' => __( 'Faculty', 'uwsp' ), /* This is the Title of the Group */
            'singular_name' => __( 'Faculty', 'uwsp' ), /* This is the individual type */
            'all_items' => __( 'All Faculty', 'uwsp' ), /* the all items menu item */
            'add_new' => __( 'Add New', 'uwsp' ), /* The add new menu item */
            'add_new_item' => __( 'Add A New Faculty', 'uwsp' ), /* Add New Display Title */
            'edit' => __( 'Edit', 'uwsp' ), /* Edit Dialog */
            'edit_item' => __( 'Edit Faculty', 'uwsp' ), /* Edit Display Title */
            'new_item' => __( 'New Faculty', 'uwsp' ), /* New Display Title */
            'view_item' => __( 'View This Faculty', 'uwsp' ), /* View Display Title */
            'search_items' => __( 'Search Faculty', 'uwsp' ), /* Search Custom Type Title */
            'not_found' =>  __( 'Nothing found in the Database.', 'uwsp' ), /* This displays if there are no entries yet */
            'not_found_in_trash' => __( 'Nothing found in Trash', 'uwsp' ), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
            ), /* end of labels array */
            'description' => __( 'Whether you’re looking for commentary on breaking trends shaping the world or a background briefing, our Faculty are available to help.', 'uwsp' ), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => 'expert',
            'menu_position' => 20, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => 'dashicons-groups', /* the icon for the custom post type menu */
            'rewrite'   => array( 'slug' => 'our-faculty', 'with_front' => false ), /* you can specify its url slug */
            'taxonomies' => array( 'uwsp_faculty_subjects' ),
            'has_archive' => true, /* you can rename the slug here */
            'capability_type' => 'page',
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'show_in_rest' => true,
            'supports' => array( 'title', 'editor', 'page-attributes', 'revisions', 'thumbnail', 'excerpt')
        ) /* end of options */
    ); /* end of register post type */
}
// adding the function to the Wordpress init
add_action( 'init', 'custom_post_faculty');
// now let's add a custom taxonomy for the experts
register_taxonomy( 'uwsp_faculty_departments',
    array('uwsp_faculty'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
        'labels' => array(
            'name' => __( 'Department', 'uwsp' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Department', 'uwsp' ), /* single taxonomy name */
            'search_items' =>  __( 'Search Departments', 'uwsp' ), /* search title for taxomony */
            'all_items' => __( 'All Faculty Departments', 'uwsp' ), /* all title for taxonomies */
            'edit_item' => __( 'Edit Faculty Department', 'uwsp' ), /* edit custom taxonomy title */
            'update_item' => __( 'Update Faculty Department', 'uwsp' ), /* update title for taxonomy */
            'add_new_item' => __( 'Add New Faculty Department', 'uwsp' ), /* add new title for taxonomy */
            'new_item_name' => __( 'New Faculty Department', 'uwsp' ) /* name title for taxonomy */
        ),
        'show_admin_column' => true,
        'show_ui' => true,
        'show_in_quick_edit' => false,
        'show_in_menu' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'faculty-departments' ),
    )
);

?>