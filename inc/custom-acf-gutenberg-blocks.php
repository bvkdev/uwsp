<?php
// Add Custom Block Categories
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );

function my_plugin_block_categories( $categories, $post ) {
    // Check function exists.
    if( function_exists('my_plugin_block_categories') ) {

        return array_merge(

            array(
                array(
                    'slug' => 'bb-custom-blocks',
                    'title' => __( 'Custom Blocks', 'bonebox-base' ),

                ),
            ),
            $categories
        );

    } // end check if exists
}

// Register Custom ACF Blocks
// add_filter('acf/register_block_type_args', function($args) {
    // $user = wp_get_current_user();
    // if ( ! array_intersect(array('administrator'), $user->roles) ) { return; }
// });

add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {
    if ( is_admin() ) {
        $user = wp_get_current_user();
        // if ( ! array_intersect(array('administrator'), $user->roles) ) { return; }
    }
    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register a Posts block.
        acf_register_block_type(array(
            'name'              => 'bb_featured_posts',
            'title'             => __('Featured Posts'),
            'description'       => __('Display a selection of posts by taxonomy term.'),
            'render_template'   => '/template-parts/blocks/bb-posts-block.php',
            'category'          => 'bb-custom-blocks',
            'icon'              => 'forms',
            'keywords'          => array( 'posts', 'latest', 'recent', 'stories' ),
            'align'             => true,
            'supports'          => array(
                'align' => true,
                'jsx' => true
            ),
            //'example'  => array(
            //    'attributes' => array(
            //        'mode' => 'preview',
            //    )
            //)
        )); // End registration


        // register a Expert Panel block.
        acf_register_block_type(array(
            'name'              => 'staff_panel',
            'title'             => __('Staff Panel'),
            'description'       => __('Display a selection of Staff'),
            'render_template'   => '/template-parts/blocks/bb-staff-panel.php',
            'category'          => 'bb-custom-blocks',
            'icon'              => 'businessman',
            'keywords'          => array( 'expert', 'experts' ),
            'align'             => true,
            'supports'          => array(
                'align' => true,
                'jsx' => true
            ),
            //'example'  => array(
            //    'attributes' => array(
            //        'mode' => 'preview',
            //    )
        ));   


        // register a Expert Panel block.
        acf_register_block_type(array(
            'name'              => 'statistics',
            'title'             => __('Statistics'),
            'description'       => __('Display a selection of Staff'),
            'render_template'   => '/template-parts/blocks/bb-statistics.php',
            'category'          => 'bb-custom-blocks',
            'icon'              => 'calculator',
            'keywords'          => array( 'statistic', 'statistics' ),
            'align'             => true,
            'supports'          => array(
                'align' => true,
                'jsx' => true
            ),
            //'example'  => array(
            //    'attributes' => array(
            //        'mode' => 'preview',
            //    )
            //)
        )); 



    } // end check if exists
}

?>
