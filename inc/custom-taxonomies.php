<?php

// Let's add a custom taxonomy for organizing 'program' pages into degree levels
register_taxonomy( 'bb_program_degrees', 
    array('page'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
        'labels' => array(
            'name' => __( 'Program Degrees', 'bonebox-base' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Degree Level', 'bonebox-base' ), /* single taxonomy name */
            'search_items' =>  __( 'Search Degree Levels', 'bonebox-base' ), /* search title for taxomony */
            'all_items' => __( 'All Degree Levels', 'bonebox-base' ), /* all title for taxonomies */
            'edit_item' => __( 'Edit Degree Levels', 'bonebox-base' ), /* edit custom taxonomy title */
            'update_item' => __( 'Update Degree Level', 'bonebox-base' ), /* update title for taxonomy */
            'add_new_item' => __( 'Add New Degree Level', 'bonebox-base' ), /* add new title for taxonomy */
            'new_item_name' => __( 'New Degree Level', 'bonebox-base' ) /* name title for taxonomy */
        ),
        'show_admin_column' => true, 
        'show_ui' => true,
        'show_in_quick_edit' => true,
        'show_in_menu' => true,
        'show_in_rest' => false,
        'show_in_nav_menus' => false,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'degrees' ),
    )
);

// Let's add a custom taxonomy for organizing 'program' pages into campuses
register_taxonomy( 'bb_program_campus', 
    array('page'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
        'labels' => array(
            'name' => __( 'Program Campuses', 'bonebox-base' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Program Campus', 'bonebox-base' ), /* single taxonomy name */
            'search_items' =>  __( 'Search Program Campuses', 'bonebox-base' ), /* search title for taxomony */
            'all_items' => __( 'All Program Campuses', 'bonebox-base' ), /* all title for taxonomies */
            'edit_item' => __( 'Edit Program Campuses', 'bonebox-base' ), /* edit custom taxonomy title */
            'update_item' => __( 'Update Program Campuses', 'bonebox-base' ), /* update title for taxonomy */
            'add_new_item' => __( 'Add New Program Campus', 'bonebox-base' ), /* add new title for taxonomy */
            'new_item_name' => __( 'New Program Campus', 'bonebox-base' ) /* name title for taxonomy */
        ),
        'show_admin_column' => true, 
        'show_ui' => true,
        'show_in_quick_edit' => true,
        'show_in_menu' => true,
        'show_in_rest' => false,
        'show_in_nav_menus' => false,
        'query_var' => true,
        //'rewrite' => array( 'slug' => 'topic' ),
    )
);

// Let's add a custom taxonomy for organizing 'program' pages into delivery formats
register_taxonomy( 'bb_program_format', 
    array('page'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
        'labels' => array(
            'name' => __( 'Program Delivery Modes', 'bonebox-base' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Program Delivery Mode', 'bonebox-base' ), /* single taxonomy name */
            'search_items' =>  __( 'Search Delivery Modes', 'bonebox-base' ), /* search title for taxomony */
            'all_items' => __( 'All Delivery Modes', 'bonebox-base' ), /* all title for taxonomies */
            'edit_item' => __( 'Edit Delivery Modes', 'bonebox-base' ), /* edit custom taxonomy title */
            'update_item' => __( 'Update Delivery Modes', 'bonebox-base' ), /* update title for taxonomy */
            'add_new_item' => __( 'Add New Delivery Mode', 'bonebox-base' ), /* add new title for taxonomy */
            'new_item_name' => __( 'New Delivery Mode', 'bonebox-base' ) /* name title for taxonomy */
        ),
        'show_admin_column' => true, 
        'show_ui' => true,
        'show_in_quick_edit' => true,
        'show_in_menu' => true,
        'show_in_rest' => false,
        'show_in_nav_menus' => false,
        'query_var' => true,
        //'rewrite' => array( 'slug' => 'topic' ),
    )
);

// Let's add a custom taxonomy for organizing 'program' pages into interest key words
register_taxonomy( 'bb_program_interests', 
    array('page'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
        'labels' => array(
            'name' => __( 'Program Interests', 'bonebox-base' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Program Interest', 'bonebox-base' ), /* single taxonomy name */
            'search_items' =>  __( 'Search Program Interests', 'bonebox-base' ), /* search title for taxomony */
            'all_items' => __( 'All Program Interests', 'bonebox-base' ), /* all title for taxonomies */
            'edit_item' => __( 'Edit Program Interests', 'bonebox-base' ), /* edit custom taxonomy title */
            'update_item' => __( 'Update Program Interests', 'bonebox-base' ), /* update title for taxonomy */
            'add_new_item' => __( 'Add New Program Interest', 'bonebox-base' ), /* add new title for taxonomy */
            'new_item_name' => __( 'New Program Interest', 'bonebox-base' ) /* name title for taxonomy */
        ),
        'show_admin_column' => true, 
        'show_ui' => true,
        'show_in_quick_edit' => true,
        'show_in_menu' => true,
        'show_in_rest' => false,
        'show_in_nav_menus' => false,
        'query_var' => true,
        //'rewrite' => array( 'slug' => 'topic' ),
    )
);

?>