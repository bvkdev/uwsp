<?php
/**
 *
 * Template Name: Overview Blocks Page
 *
 */

get_header();
?>
<?php
    $page_level = count(get_post_ancestors( $post->ID )) + 1;
    if( $page_level >= 1 && !is_front_page() ) {
        $page_headline = get_field("marketing_headline");
?>
        <div class="b-secondary-header">
            <div class="b-secondary-header__image"><?php getwid_base_post_thumbnail() ?></div>
            <h2 class="b-secondary-header__headline"><?php echo $page_headline; ?></h2>

            <div class="b-secondary-header-menubox">

                <h1 class="b-secondary-header-menubox__page-title">
                    <?php the_title(); ?>
                </h1>
                <div class="b-secondary-header-menubox__menu-container">
                    <?php wp_nav_menu(array(
                        'container' => false,                           // remove nav container
                        'container_class' => false,                 // class of container (should you choose to use it)
                        'menu' => __( 'The SubMenu', 'bonebox' ),  // nav name
                        'menu_id' => 'b-secondary-header-menu', 
                        'menu_class' => 'menu b-secondary-header-menubox__menu',               // adding custom nav class
                        'theme_location' => 'menu-1.1',                 // where it's located in the theme
                        'before' => '',                                 // before the menu
                        'after' => '',                                  // after the menu
                        'link_before' => '',                            // before each link
                        'link_after' => '',                             // after each link
                        'depth' => 1,                                   // limit the depth of the nav
                        'fallback_cb' => '',                            // fallback function (if there is one)
                        'sub_menu' => true                          
                        )); ?>
                </div>
            </div>
        </div>
<?php
    } //end check for page level to include header
?>
	<div id="primary" class="content-area acf-page">
		<main id="main" class="site-main site-main_acf-blocks">

			<?php get_template_part('template-parts/acf-blocks/loop-flexible-content'); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
