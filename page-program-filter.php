<?php
/* Template Name: Program Filter */

/**
 * The template for displaying all pages with no sidebar
**/

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
        <header class="entry-header">
            <div class="entry-header__title-wrap">
                <span class="page-parent"><?= get_the_title($post->post_parent) ?></span>
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            </div>
            <?php
            getwid_base_post_thumbnail('page-header-half-width');
            if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb( '<div id="breadcrumbs" class="uwsp-breadcrumbs"><div class="uwsp-breadcrumbs__inner">','</div></div>' );
            }
            ?>
        </header><!-- .entry-header -->
        <div class="b-columns t-all p-all wp-no-sidebar">
            <?php while ( have_posts() ) :
				the_post(); ?>

				<div class="program-filter-wrap">
					<section class="uwsp-sidebar p-all t-all d-1of3">
				        <aside class="widget">
				        	<div class="search-wrap">
				        		<span class="lnr lnr-magnifier"></span>
				        		<input type="text" id="SearchFilter" placeholder="Search" />
				        	</div>
				            <!-- <h2 class="widget-title">Academics</h2> -->
				            <div id="uwsp-sidebar-nav" class="uwsp-sidebar-nav">
				            	<div class="filter-section open">
				            		<button class="filter-header">
				            			<div class="c-wrap">
							            	<h3 class="sb-title">Program Level</h3>
							            	<div class="reset-wrap">
							            		<span class="lnr lnr-menu"></span>
							            		<!-- <button id="resetPrograms">Reset</button> -->
							            	</div>
						            	</div>
						            </button>
									<?php
										$degrees = get_terms( array(
									    'taxonomy' => 'bb_program_degrees',
									    'hide_empty' => false
									) );
									
									if ( !empty($degrees) ) : ?>
										<div class="filter-content">
										    <ul>
										    <?php foreach( $degrees as $category ) { if( $category->parent == 0 ) { ?>
										        	<li data-degree="<?php echo esc_attr( $category->name ); ?>">
										        		<label>
														    <input id="" type="checkbox" value="<?php echo str_replace(' ', '', $category->name); ?>" name="degrees" class="filter-item" />
														    <span class="check"></span>
														    <?php echo $category->name ; ?>
														</label>
									        		</li>
										     <?php  }
										     } ?>
										     	<li><button class="reset-filter" data-type="degrees" >Reset</button></li>
										    </ul>
										</div>
									<?php echo $output; endif; ?>

					            </div>
				            	<div class="filter-section open">
				            		<button class="filter-header">
				            			<div class="c-wrap">
							            	<h3 class="sb-title">Format</h3>
							            	<div class="reset-wrap">
							            		<span class="lnr lnr-menu"></span>
							            		<!-- <button id="resetPrograms">Reset</button> -->
							            	</div>
						            	</div>
						            </button>
									<?php
										$degrees = get_terms( array(
									    'taxonomy' => 'bb_program_format',
									    'hide_empty' => false
									) );
									 
									if ( !empty($degrees) ) : ?>
										<div class="filter-content">
										    <ul>
										    <?php foreach( $degrees as $category ) { if( $category->parent == 0 ) { ?>
										        	<li data-degree="<?php echo esc_attr( $category->name ); ?>">
										        		<label>
														    <input id="" type="checkbox" value="<?php echo str_replace(' ', '', $category->name); ?>" name="format" class="filter-item" />
														    <span class="check"></span>
														    <?php echo $category->name ; ?>
														</label>
									        		</li>
										     <?php  }
										     } ?>
										     	<li><button class="reset-filter" data-type="formats" >Reset</button></li>
										    </ul>
										</div>
									<?php echo $output; endif; ?>           	
					            </div>
				            	<div class="filter-section open">
				            		<button class="filter-header">
				            			<div class="c-wrap">
							            	<h3 class="sb-title">Campus</h3>
							            	<div class="reset-wrap">
							            		<span class="lnr lnr-menu"></span>
							            		<!-- <button id="resetPrograms">Reset</button> -->
							            	</div>
						            	</div>
						            </button>
									<?php
										$degrees = get_terms( array(
									    'taxonomy' => 'bb_program_campus',
									    'hide_empty' => false
									) );
									 
									if ( !empty($degrees) ) : ?>
										<div class="filter-content">
										    <ul>
										    <?php foreach( $degrees as $category ) { if( $category->parent == 0 ) { ?>
										        	<li data-campus="<?php echo esc_attr( $category->name ); ?>">
										        		<label>
														    <input id="" type="checkbox" value="<?php echo str_replace(' ', '', $category->name); ?>" name="campus" class="filter-item" />
														    <span class="check"></span>
														    <?php echo $category->name ; ?>
														</label>
									        		</li>
										     <?php  }
										     } ?>
										     	<li><button class="reset-filter" data-type="campuses" >Reset</button></li>

										    </ul>
										</div>
									<?php echo $output; endif; ?>           	
					            </div>
				            	<div class="filter-section">
				            		<button class="filter-header">
				            			<div class="c-wrap">
							            	<h3 class="sb-title">Interests</h3>
							            	<div class="reset-wrap">
							            		<span class="lnr lnr-menu"></span>
							            		<!-- <button id="resetPrograms">Reset</button> -->
							            	</div>
						            	</div>
						            </button>
									<?php
										$degrees = get_terms( array(
									    'taxonomy' => 'bb_program_interests',
									    'hide_empty' => false
									) );
									 
									if ( !empty($degrees) ) : ?>
										<div class="filter-content">
										    <ul>
										    <?php foreach( $degrees as $category ) { if( $category->parent == 0 ) { ?>
										        	<li data-interests="<?php echo esc_attr( $category->name ); ?>">
										        		<label>
														    <input id="" type="checkbox" value="<?php echo str_replace(' ', '', $category->name); ?>" name="interests" class="filter-item" />
														    <span class="check"></span>
														    <?php echo $category->name ; ?>
														</label>
									        		</li>
										     <?php  }
										     } ?>
										     	<li><button class="reset-filter" data-type="interests" >Reset</button></li>
										    </ul>
										</div>
									<?php echo $output; endif; ?>           	
					            </div>

				            </div>
				        </aside>
				    </section>
				    <section class="program-section">
				    	<div class="filter-items">
						  	<div class="box-sizer"></div>
						  	<div class="gutter-sizer"></div>
					 <!--        <div > -->
							<?php $args = array(
							    'sort_order' => 'asc',
							    'sort_column' => 'post_title',
							    'post_type' => 'page',
							    'post_status' => '',
							    'numberposts'	=> -1,
							    'meta_query' => array(
							    	'relation'		=> 'AND',
						            array(
						                'key' => 'page_type', 
						                'value' => 'program', 
						                'compare' => 'LIKE'
						            )
							    )
							); 

							$pages = get_posts($args); 
							?>
							<?php
							    foreach ($pages as $page) { ?>
						    	<?php $degree_levels_terms = get_field( 'degree_levels', $page->ID ); ?>
						    	<?php $program_campuses_terms = get_field( 'program_campuses', $page->ID ); ?>
						    	<?php $mode_of_delivery_terms = get_field( 'mode_of_delivery', $page->ID ); ?>
						    	<?php $interests_key_words_terms = get_field( 'interests_key_words', $page->ID ); ?>

							    <div class="pd-program box" 
							    	data-degrees="<?php if ( $degree_levels_terms ): foreach ( $degree_levels_terms as $degree_levels_term ): ?><?php echo str_replace(' ', '', $degree_levels_term->name); ?> <?php endforeach; endif; ?>" 
							    	data-format="<?php if ( $mode_of_delivery_terms ): foreach ( $mode_of_delivery_terms as $mode_of_delivery_term ): ?><?php echo str_replace(' ', '', $mode_of_delivery_term->name); ?> <?php endforeach; endif; ?>"
							    	data-interests="<?php if ( $interests_key_words_terms ): foreach ( $interests_key_words_terms as $interests_key_words_term ): ?><?php echo str_replace(' ', '', $interests_key_words_term->name); ?> <?php endforeach; endif; ?>"
							    	data-campuses="<?php if ( $program_campuses_terms ): foreach ( $program_campuses_terms as $program_campuses_term ): ?><?php echo str_replace(' ', '', $program_campuses_term->name); ?> <?php endforeach; endif; ?>"
							    	data-title="<?php echo $page->post_title; ?>"
							    	>
							    	<button class="toggle-program">
							    		<div class="title"><?php echo $page->post_title; ?></div>
							    		<div class="icon"></div>
							    	</button>
							    	<div class="description">
							    		<div class="inner">
								    		<!-- Excerpt -->
								    		<p><?php echo $page->post_excerpt; ?></p>
								    		<!-- Avail -->
								    		<div class="title">Available as</div>
								    		<p>
								    			<?php if ( $mode_of_delivery_terms ): foreach ( $mode_of_delivery_terms as $mode_of_delivery_term ): ?>
								    				<span class="avail"><?php echo $mode_of_delivery_term->name; ?></span>
								    			<?php endforeach; endif; ?>
								    		</p>
								    		<!-- Link -->
								    		<a href="<?php echo $page->guid; ?>" class="b-buttons-group__button b-buttons-group__button_1 b-side-text__button b-side-text__button_1" title="Link to program details for <?php echo $page->post_title; ?>">Program details</a>
								    	</div>
							    	</div>
							    </div>
							<?php } ?>
				        </div>

						<div class="empty-filter">
							<h2>We're sorry, nothing meets your criteria.</h2> <p>Please update you filter selection or reset them here.</p>
							<button class="b-buttons-group__button b-buttons-group__button_1 b-side-text__button b-side-text__button_1" id="ResetFilters">Reset Filters</button>
						</div>	 
				    </section>
				</div>
	<?php
			endwhile; // End of the loop.
			?>
        </div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();


