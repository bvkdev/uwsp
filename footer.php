<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package getwid_base
 */

?>

</div><!-- #content -->
<footer id="social-footer" class="site-footer site-footer_social">
    <?php $social_icons_header = get_field("social_icons_header", "options"); ?>
    <h2><?php echo $social_icons_header; ?></h2>
    <?php get_template_part("template-parts/loop-social-options"); ?>
</footer>
<?php
get_sidebar();
?>
<footer id="locations-footer" class="site-footer site-footer_locations">
	<!-- <div class="b-section__wrap-outer"> -->
	<?php if ( have_rows( 'campus_locations', 'option' ) ) : ?>
		<div class="locations">
			<?php while ( have_rows( 'campus_locations', 'option' ) ) : the_row(); ?>
				<div class="location">
					<?php $logo = get_sub_field( 'logo' ); ?>
					<?php if ( $logo ) { ?>
						<div class="logo-wrap">
							<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
						</div>
					<?php } ?>		
					<div class="address">
						<?php the_sub_field( 'address' ); ?>
					</div>
					<a href="tel:<?php the_sub_field( 'phone' ); ?>" class="phone"><?php the_sub_field( 'phone' ); ?></a>
					<a href="mailto:<?php the_sub_field( 'email' ); ?>" class="email"><?php the_sub_field( 'email' ); ?></a>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
	<div class="additional-info">
		<div class="footer-map">
		<?php $campus_map = get_field( 'campus_map', 'option' ); ?>
		<?php if ( $campus_map ) { ?>
			<img src="<?php echo $campus_map['url']; ?>" alt="<?php echo $campus_map['alt']; ?>" />
		<?php } ?>
		</div>
		<div class="copy">
			<?php the_field( 'additional_info', 'option' ); ?>
		</div>
	</div>
</footer>
<footer id="colophon" class="site-footer site-footer_copyright">
	<?php

	$show_site_info = (bool) get_theme_mod( 'getwid_base_show_footer_text', true );
	if ( $show_site_info ):
		?>
		<div class="site-info">
            <p>
			<?php
			$dateObj           = new DateTime;
			$current_year      = $dateObj->format( "Y" );
			$site_info_default = apply_filters( 'getwid_base_site_info',
				/* translators: %1$s: current year. */
				esc_html_x( 'Copyright &copy; %1$s.  All Rights Reserved.', 'Default footer text. %1$s - current year.', 'getwid-base' )
			);
			$site_info         = get_theme_mod( 'getwid_base_footer_text', false ) ? get_theme_mod( 'getwid_base_footer_text' ) : $site_info_default;

			echo wp_kses_post(
				sprintf(
					$site_info,
					$current_year
				)
			);
			?>
        </p>
		</div>
        <div class="footer-utility-menu">
            <?php
                wp_nav_menu( array(
                    'theme_location'  => 'footer-utility',
                    'menu_id'         => 'footer-utility',
                    'menu_class'      => 'menu b-footer-utility',
                    'container_class' => false
                ) );
            ?>
        </div>
	<?php
	endif;
	?>
</footer><!-- #colophon --></div><!-- #page -->

<?php wp_footer(); ?>

</body></html>
