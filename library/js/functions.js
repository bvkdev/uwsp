
(function ($) {

    "use strict";

    var search_modal = $('#search-modal'),
        search_field = search_modal.find('.search-field');

    $('#search-toggle, #close-search-modal, #search-toggle-mobile').on('click', function (e) {

        e.preventDefault();

        search_modal.toggleClass('opened');

        if (search_modal.hasClass('opened')) {
            search_field.focus();
        } else {
            search_field.blur();
            $("#search-toggle").focus();
        }

    });

    search_field.keydown(function (e) {    
        if (e.which == 9) {      
            $("#close-search-modal").focus();
            e.preventDefault();
        }
    });

    $('#close-search-modal').keydown(function (e) {    
        if (e.which == 9) {      
            search_field.focus();
            e.preventDefault();
        }
    });

    var masthead, menuToggle, siteNavigation, siteHeaderMenu;

    function initMainNavigation(container) {
        // Add dropdown toggle that displays child menu items.
        var dropdownToggle = $('<button />', {
            'class': 'dropdown-toggle',
            'aria-expanded': false,
            'html': '<span class="lnr lnr-chevron-down"></span>'
        });

        container.find('.menu-item-has-children > a').after(dropdownToggle);

        // Toggle buttons and submenu items with active children menu items.
        container.find('.current-menu-ancestor > button').addClass('toggled-on');
        container.find('.current-menu-ancestor > .children, .current-menu-ancestor > .sub-menu').addClass('toggled-on');

        // Add menu items with submenus to aria-haspopup="true".
        container.find('.menu-item-has-children').attr('aria-haspopup', 'true');

        container.on('click', '.dropdown-toggle', function (e) {
            var _this = $(this);

            e.preventDefault();
            _this.toggleClass('toggled-on');
            _this.closest('.menu-item-has-children').toggleClass('toggled-on');
            _this.next('.children, .sub-menu').toggleClass('toggled-on');

            _this.attr('aria-expanded', _this.attr('aria-expanded') === 'false' ? 'true' : 'false');
        });
    }

    initMainNavigation($('.main-navigation'));

    masthead = $('#masthead');
    menuToggle = masthead.find('#menu-toggle');

    // Enable menuToggle.
    (function () {
        // Return early if menuToggle is missing.
        if (!menuToggle.length) {
            return;
        }

        // Add an initial values for the attribute.
        menuToggle.add(siteNavigation).attr('aria-expanded', 'false');

        menuToggle.on('click', function () {
            $(this).add(siteHeaderMenu).toggleClass('toggled-on');
            $(this).add(siteNavigation).attr('aria-expanded', $(this).add(siteNavigation).attr('aria-expanded') === 'false' ? 'true' : 'false');
        });
    })();

    function subMenuPosition() {
        $('.sub-menu').each(function () {
            $(this).removeClass('toleft');
            if (($(this).parent().offset().left + $(this).parent().width() - $(window).width() + 230) > 0) {
                $(this).addClass('toleft');
            }
        });
    }

    function prependElement(container, element) {
        if (container.firstChild) {
            return container.insertBefore(element, container.firstChild);
        } else {
            return container.appendChild(element);
        }
    }

    function showButton(element) {
        // classList.remove is not supported in IE11
        element.className = element.className.replace('is-empty', '');
    }

    function hideButton(element) {
        // classList.add is not supported in IE11
        if (!element.classList.contains('is-empty')) {
            element.className += ' is-empty';
        }
    }

    function getAvailableSpace(button, container) {
        return container.offsetWidth - button.offsetWidth - 85;
    }

    function isOverflowingNavivation(list, button, container) {
        return list.offsetWidth > getAvailableSpace(button, container);
    }

    function addItemToVisibleList(toggleButton, container, visibleList, hiddenList) {
        if (getAvailableSpace(toggleButton, container) > breaks[breaks.length - 1]) {
            // Move the item to the visible list
            visibleList.appendChild(hiddenList.firstChild);
            breaks.pop();
            addItemToVisibleList(toggleButton, container, visibleList, hiddenList);
        }
    }

    $('.filter-header').click(function(e) {
        e.preventDefault();
        if( $(this).closest('.filter-section').hasClass('open') ) {
            $(this).closest('.filter-section').removeClass('open');
        } else {
            // $('.filter-section.open').removeClass('open');
            $(this).closest('.filter-section').addClass('open');
        }
    });

    function programsAndDegreesFilter() {
        var programFilter = $('.filter-items');
        var degrees = [];
        var formats = [];
        var interests = [];
        var campuses = [];
        var searchVal = '';

        if (programFilter) {
            var $grid = programFilter.masonry({
                columnWidth: '.box-sizer',
                gutter: '.gutter-sizer',
                itemSelector: '.box'
            });

            $(document).on('click', '.toggle-program', function(e) {
                e.preventDefault();
                var parent = $(this).closest('.pd-program');

                if( parent.hasClass('open') ) {
                    parent.removeClass('open');
                } else {
                    $('.pd-program.open').removeClass('open');
                    parent.toggleClass('open');
                }
                // setTimeout(function() {
                    $grid.masonry();
                // }, 50);
                
            });

            $('.filter-item').change(function() {
                degrees = [];
                formats = [];
                interests = [];
                campuses = [];
                $('input[type="checkbox"][name="degrees"]:checked').map(function() { 
                    degrees.push(this.value);
                });
                $('input[type="checkbox"][name="format"]:checked').map(function() { 
                    formats.push(this.value);
                });
                $('input[type="checkbox"][name="interests"]:checked').map(function() { 
                    interests.push(this.value);
                });
                $('input[type="checkbox"][name="campus"]:checked').map(function() { 
                    campuses.push(this.value);
                });
                toggleProgramsDegrees();
            });

            $('#SearchFilter').on("input", function() {
                searchVal = $(this).val().toLowerCase();
                toggleProgramsDegrees();
            });

            function filterArray(array, items) {
                var valid = false;
                for(var i = 0; i < array.length; i++) {
                    if($.inArray(array[i], items) !== -1) {
                        valid = true;

                    }
                }
                return valid;
            }

            function toggleProgramsDegrees() {
                var isEmpty = true;
                $('.pd-program').each(function() {
                    var $dis = $(this),
                        element_degrees = $(this).attr("data-degrees").split(" "),
                        element_formats = $(this).attr("data-format").split(" "),
                        element_interests = $(this).attr("data-interests").split(" "),
                        element_campuses = $(this).attr("data-campuses").split(" "),
                        title = $(this).attr("data-title").toLowerCase(),
                        isValid = true,
                        hasDegree = false,
                        hasFormat = false,
                        hasInterest = false,
                        hasCampus = false,
                        matchesSearch = true;

                    if(degrees.length > 0) {
                        hasDegree = filterArray(element_degrees, degrees);
                        if(!hasDegree) {
                            isValid = false;
                        }
                    }

                    if(formats.length > 0) {
                        hasFormat = filterArray(element_formats, formats);
                        if(!hasFormat) {
                            isValid = false;
                        }
                    }

                    if(interests.length > 0) {
                        hasInterest = filterArray(element_interests, interests);
                        if(!hasInterest) {
                            isValid = false;
                        }
                    }

                    if(campuses.length > 0) {
                        hasCampus = filterArray(element_campuses, campuses);
                        if(!hasCampus) {
                            isValid = false;
                        }
                    }

                    if(isValid == false) {
                        $dis.hide();
                    } else {
                        // Nothing selected
                        isEmpty = false;
                        $dis.show();
                    }

                    if (searchVal.length && title.indexOf(searchVal) != 0) {
                        matchesSearch = false;
                    }

                    if(matchesSearch == false) {
                        $dis.hide();
                    }
                    // Reset grid

                });
                if(isEmpty) {
                    toggleEmpty(true);
                } else {
                    toggleEmpty(false);
                }

                // setTimeout(function() {
                $grid.masonry();
                // }, 150);
            }


            $('.reset-filter').click(function(e) {
                var type = $(this).attr('data-type');
                if(type == 'interests') {
                    interests = [];
                } else if(type == 'campuses') {
                    campuses = [];
                } else if(type == 'formats') {
                    formats = [];
                } else if(type == 'degrees') {
                    degrees = [];
                }
                
                $('input[type="checkbox"][name="'+type+'"]').removeAttr('checked');
                toggleProgramsDegrees();
            });

        }
        function toggleEmpty(empty) {
            if(empty) {
                $('.empty-filter').show();
                $('.filter-items').hide();
            } else {
                $('.empty-filter').hide();
                $('.filter-items').show();
            }
        }



        $('#ResetFilters').click(function(e) {
            e.preventDefault();
            searchVal = '';
            $('#SearchFilter').val('');
            $('input[type="checkbox"][name="degrees"]').removeAttr('checked');
            $('input[type="checkbox"][name="format"]').removeAttr('checked');
            $('input[type="checkbox"][name="interests"]').removeAttr('checked');
            $('input[type="checkbox"][name="campus"]').removeAttr('checked');
            $('.empty-filter').hide();
            $('.filter-items').show();
            $('.pd-program').show();
            $grid.masonry();
        });
    }



    var navContainer = document.querySelector('.main-navigation-wrapper');
    var breaks = [];

    if (navContainer) {
        function updateNavigationMenu(container) {

            if (!container.parentNode.querySelector('.primary-menu[id]')) {
                return;
            }

            // Adds the necessary UI to operate the menu.
            var visibleList = container.parentNode.querySelector('.primary-menu[id]');
            var hiddenList = visibleList.parentNode.nextElementSibling.querySelector('.hidden-links');
            var toggleButton = visibleList.parentNode.nextElementSibling.querySelector('.primary-menu-more-toggle');

            if ((window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) <= 991) {
                while (breaks.length > 0) {
                    visibleList.appendChild(hiddenList.firstChild);
                    breaks.pop();
                    addItemToVisibleList(toggleButton, container, visibleList, hiddenList);
                }
                return;
            }

            if (isOverflowingNavivation(visibleList, toggleButton, container)) {
                // Record the width of the list
                breaks.push(visibleList.offsetWidth);
                // Move last item to the hidden list
                prependElement(hiddenList, !visibleList.lastChild || null === visibleList.lastChild ? visibleList.previousElementSibling : visibleList.lastChild);
                // Show the toggle button
                showButton(toggleButton);

            } else {

                // There is space for another item in the nav
                addItemToVisibleList(toggleButton, container, visibleList, hiddenList);

                // Hide the dropdown btn if hidden list is empty
                if (breaks.length < 2) {
                    hideButton(toggleButton);
                }

            }

            // Recur if the visible list is still overflowing the nav
            if (isOverflowingNavivation(visibleList, toggleButton, container)) {
                updateNavigationMenu(container);
            }

        }

        document.addEventListener('DOMContentLoaded', function () {

            updateNavigationMenu(navContainer);

            // Also, run our priority+ function on selective refresh in the customizer
            var hasSelectiveRefresh = (
                'undefined' !== typeof wp &&
                wp.customize &&
                wp.customize.selectiveRefresh &&
                wp.customize.navMenusPreview.NavMenuInstancePartial
            );

            if (hasSelectiveRefresh) {
                // Re-run our priority+ function on Nav Menu partial refreshes
                wp.customize.selectiveRefresh.bind('partial-content-rendered', function (placement) {

                    var isNewNavMenu = (
                        placement &&
                        placement.partial.id.includes('nav_menu_instance') &&
                        'null' !== placement.container[0].parentNode &&
                        placement.container[0].parentNode.classList.contains('main-navigation')
                    );

                    if (isNewNavMenu) {
                        updateNavigationMenu(placement.container[0].parentNode);
                    }
                });
            }
        });

        window.addEventListener('load', function () {
            updateNavigationMenu(navContainer);
            subMenuPosition();
            programsAndDegreesFilter();
        });

        var timeout;

        window.addEventListener('resize', function () {
            function checkMenu() {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = undefined;
                }

                timeout = setTimeout(
                    function () {
                        updateNavigationMenu(navContainer);
                        subMenuPosition();
                    },
                    100
                );
            }

            checkMenu();
            subMenuPosition();
        });

        updateNavigationMenu(navContainer);
        subMenuPosition();
    }

})(jQuery);