<?php
/**
 * This sidebar has two sections.
 *
 * The first retrieves the current page's parent's direct ancestors. The current
 * page then has its direct ancestors added to it to provide three generations.
 *
 * The second menu lists the top-level parent's direct ancestors no matter how
 * deep the page is in the hierarchy.
 */
$menu = [];
$parent = ($post->post_parent !== 0)
    ? get_post($post->post_parent)
    : $post;
?>
<section class="uwsp-sidebar p-all t-all d-1of3">
    <?php
    $progeny = get_pages([
        'post_type'   => 'page',
        'post_status' => 'publish',
        'parent'      => $post->post_parent,
        'sort_column'  => 'menu_order',
    ]);
    foreach ( $progeny as $item ) {
        $menu[$item->ID] = $item;
        $scions = get_pages([
            'post_type'   => 'page',
            'post_status' => 'publish',
            'parent'      => $item->ID,
            'sort_column'  => 'menu_order',
        ]);
        if ( $scions ) {
            $menu[$item->ID]->children = $scions;
        }
    }
    if ( $menu[$post->ID] && isset($menu[$post->ID]->children) ) {
        foreach ( $menu[$post->ID]->children as $child ) {
            $children = get_pages([
                'post_type'   => 'page',
                'post_status' => 'publish',
                'parent'      => $child->ID,
                'sort_column'  => 'menu_order',
            ]);
            if ( $children ) {
                $child->children = $children;
            }
        }
    }
    if ( $menu ) : ?>
        <aside class="widget">
            <h2 class="widget-title"><?= $parent->post_title ?></h2>
            <ul id="uwsp-sidebar-nav" class="uwsp-sidebar-nav">
                <?php foreach ( $menu as $scion ) : ?>
                    <li<?php if ( $scion->ID == $post->ID ) { echo ' class="expanded current-page"'; } ?>>
                        <a href="<?= get_permalink($scion->ID) ?>">
                            <?= $scion->post_title; ?>
                            <?php if ( $scion->children ) : ?>
                                <i class="fas fa-chevron-right"></i>
                            <?php endif; ?>
                        </a>
                        <?php if ( $scion->children ) : ?>
                            <ul class="uwsp-sidebar-nav__sub-nav">
                                <?php foreach ( $scion->children as $scamp ) : ?>
                                    <li>
                                        <a href="<?= get_permalink($scamp->ID) ?>">
                                            <?= $scamp->post_title; ?>
                                            <?php if ( $scamp->children ) : ?>
                                                <i class="fas fa-chevron-right"></i>
                                            <?php endif; ?>
                                        </a>
                                        <?php if ( $scamp->children ) : ?>
                                            <ul class="uwsp-sidebar-nav__sub-nav">
                                                <?php foreach ( $scamp->children as $imp ) : ?>
                                                    <li><a href="<?= get_permalink($imp->ID) ?>"><?= $imp->post_title; ?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </aside>
        <script>
            var triggers = document.querySelectorAll('#uwsp-sidebar-nav i');
            [].slice.call(triggers).forEach(function(trigger) {
                trigger.addEventListener('click', function(evt) {
                    evt.preventDefault();
                    var self = evt.target;
                    self.parentNode.parentNode.classList.toggle('expanded');
                });
            });
        </script>
    <?php endif; ?>
    <?php
    if ( $parent->post_parent !== 0 ) :
        $matriarch = get_post_ancestors($post->ID);
        $matriarch = (gettype($matriarch) != 'array') ?: get_post(end($matriarch));
        $progeny = get_pages([
            'post_type'   => 'page',
            'post_status' => 'publish',
            'parent'      => $matriarch->ID,
            'sort_column'  => 'menu_order',
        ]);
        if ( $matriarch && $progeny ) : ?>
            <aside class="widget">
                <h2 class="widget-title"><?= $matriarch->post_title ?></h2>
                <?php if ( $progeny ) : ?>
                    <ul>
                        <?php foreach ( $progeny as $scion ) : ?>
                            <li><a href="<?= get_permalink($scion->ID) ?>"><?= $scion->post_title; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </aside>
        <?php endif; ?>
    <?php endif; ?>
</section>
