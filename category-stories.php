<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package getwid_base
 */
get_header();
$page = get_queried_object();
?>

<?php if ( is_paged() ) : ?>
    <?php get_template_part('template-parts/page-archive'); ?>
<?php else : ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main b-columns t-all p-all">
            <section class="story-wall d-all t-all p-all">
                <?php if ( have_posts() ) : ?>
                    <header class="page-header">
                        <h1 class="page-title"><?= $page->description ?></h1>
                        <div class="uwsp-filter">
                            <ul>
                                <!-- <li><a href="#" tab-index="-1" aria-hidden="true">all</a></li> -->
                                <?php foreach ( get_categories(['parent' => $page->term_id]) as $cat ) : ?>
                                    <li><a href="<?= get_category_link($cat) ?>"><?= $cat->name ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </header>
                    <?php
                    while ( have_posts() ) :
                        the_post();
                        get_template_part( 'template-parts/content-loop', get_post_type(), ['blurb' => 15] );
                    endwhile;
                    ?>
                <?php
                else :
                    get_template_part( 'template-parts/content', 'none' );
                endif;
                ?>
            </section>
            <button class="uwsp-load-more"><?= get_next_posts_link('view more<i class="far fa-arrow-alt-circle-right"></i>') ?></button>
        </main>
    </div>
<?php endif; ?>
<?php
get_footer();
