<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package getwid_base
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
        <header class="entry-header">
            <div class="entry-header__title-wrap">
                <span class="page-parent"><?= get_the_title($post->post_parent) ?></span>
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            </div>
    <!--         <div class="entry-half-img-wrap post-thumbnail-wrapper">
                <div class="content">
                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" role="presentation">
                </div>
            </div> -->
            <?php

            getwid_base_post_thumbnail('page-header-half-width');
            if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb( '<div id="breadcrumbs" class="uwsp-breadcrumbs"><div class="uwsp-breadcrumbs__inner">','</div></div>' );
            }
            ?>
        </header><!-- .entry-header -->
        <div class="b-columns t-all p-all">
            <?php
            get_sidebar('page');

			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'page' );

				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
        </div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
