<?php
namespace Deployer;

require 'recipe/common.php';

/*
|--------------------------------------------------------------------------
| Hosts
|--------------------------------------------------------------------------
|
| Hosts are stored in the Yaml file linked to below.
|
*/
inventory('hosts.yml');

// Required to get things running with the hosts file
// ref: https://github.com/deployphp/deployer/issues/1393#issuecomment-339372638
set('default_stage', 'staging');
set('use_relative_symlink', false);

/*
|--------------------------------------------------------------------------
| Settings
|--------------------------------------------------------------------------
*/
// Project name
set('application', 'uwsp');

// Project repository
set('repository', '{{repository}}');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// What is the server's "apache" user and group name?
set('apache_groupname', 'www-data');

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);
set('allow_anonymous_stats', false);

set('writable_use_sudo', true);

/*
|--------------------------------------------------------------------------
| Tasks
|--------------------------------------------------------------------------
|
| ... description here
|
*/
desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    // 'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

/*
|--------------------------------------------------------------------------
| Custom tasks
|--------------------------------------------------------------------------
|
| ... description here
|
*/

/**
 * Overwrite original `deploy:symlink` function for WP
 */
task('deploy:symlink', function () {
    desc('Creating symlink to release');
    run("cd {{deploy_path}}/themes && {{bin/symlink}} {{release_path}} current"); // Atomic override symlink.
    run("cd {{deploy_path}}/themes"); // Remove release link.
});

/**
 * Overwrite original `rollback` function for WP
 */
task('rollback', function () {
    desc('Rollback to previous release');
    
    $releases = get('releases_list');

    if (isset($releases[1])) {
        $releaseDir = "{{deploy_path}}/releases/{$releases[1]}";

        // Symlink to old release.
        run("cd {{deploy_path}}/themes && {{bin/symlink}} $releaseDir current");

        // Remove release
        run("rm -rf {{deploy_path}}/releases/{$releases[0]}");

        if (isVerbose()) {
            writeln("Rollback to `{$releases[1]}` release was successful.");
        }
    } else {
        writeln("<comment>No more releases you can revert to.</comment>");
    }
});

/**
 * Update ownership to shared "apache" group
 */
task('permissions:group', function () {
    run('sudo chown -R :{{apache_groupname}} {{deploy_path}}');
});
after('deploy:unlock', 'permissions:group');

// Restart PHP-FPM engine. Symlink updates won't be recognized until this is done. 
task('restart_php_fpm', function() {
    desc('Restarting PHP-FPM');
    run ('sudo /etc/init.d/php*-fpm restart');
});
after('deploy:symlink', 'restart_php_fpm');

/**
 * Execute deployment hooks only with given role. 
 * e.g., you only want to migrate a database 
 * for a given task or domain. 
 * ref: https://deployer.org/docs/tasks.html#by-roles
 */
// desc('Migrate database');
// task('migrate', function () {
//     // ...
// })->onRoles('db');

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
