<?php
/**
 * The template for displaying all single posts
 *
 * @package getwid_base
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main b-columns t-all p-all">
        <section class="uwsp-sidebar p-all t-all d-1of3">
            <?php
            if ( is_active_sidebar('Post Sidebar') ) {
                dynamic_sidebar('Post Sidebar');
            }
            ?>
        </section>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			getwid_base_the_post_navigation();

			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile;
		?>

	</main>
</div>

<?php
get_footer();
