<section class="uwsp-sidebar p-all t-all d-1of3">
    <aside class="widget">
        <h2 class="widget-title"><?= single_cat_title('', false) ?></h2>
        <ul>
            <?php foreach ( get_categories() as $cat ) : ?>
                <li><a href="<?= get_category_link($cat) ?>"><?= $cat->name ?></a></li>
            <?php endforeach; ?>
        </ul>
    </aside>
</section>
