<div id="primary" class="uwsp-archive content-area">
    <main id="main" class="site-main b-columns t-all p-all">

        <section class="uwsp-sidebar p-all t-all d-1of3">
            <aside class="widget">
                <h2 class="widget-title">Faculty</h2>
                <ul>
                    <?php foreach ( get_categories() as $cat ) : ?>
                        <li><a href="<?= get_category_link($cat) ?>"><?= $cat->name ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </aside>
        </section>

        <section class="d-2of3 t-all p-all">
            <?php if ( have_posts() ) : ?>
                <header class="page-header">
                    <h1 class="page-title">Our Faculty</h1>
                    <?php getwid_base_posts_pagination(); ?>
                </header><!-- .entry-header -->
                <div class="b-staff-panel b-staff-panel_left_intro" >
                    <div class="b-posts-wrapper b-columns" >
                        <section class="b-staff-cards b-columns p-all m-all t-2of3 d-3of4 match-heights">
                            <?php
                            /* Start the Loop */
                            while ( have_posts() ) :
                                the_post();

                                /*
                                 * Include the Post-Type-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                                 */
                                // get_template_part( 'template-parts/content-loop', get_post_type() );
                                echo "<div class='p-all m-1of2 t-1of3 d-1of3'>";
                                    get_template_part( 'template-parts/content-loop-staff' );
                                echo "</div>";
                            endwhile;
                            
                            ?>
                        </section>
                    </div>
                </div>
                <?php getwid_base_posts_pagination(); ?>
            <?php
            else :
                get_template_part( 'template-parts/content', 'none' );
            endif;
            ?>
        </section>
    </main>
</div>