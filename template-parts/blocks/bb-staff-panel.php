<?php

/**
 * Expert Panel Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
if (get_field('is_example')) :
    echo '<img src="" />';
    echo 'preveewed';
else :
// Create id attribute allowing for custom "anchor" value.
$id = 'bb_staff-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'b-staff-panel';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.
$how_many_to_display = get_field('how_many_to_display') ?: 3;
$how_many_across = get_field('how_many_across') ?: 3;
$staff_department = get_field('staff_department') ?: '';
$intro_setting = get_field('include_an_intro') ?: 'yes_intro';
$intro_placement = get_field('intro_placement') ?: 'top_intro';
//$intro_placement = 'top_intro';
// Set Background Color
$staff_section_background_color = get_field('background_color') ?: '#fff';
if($staff_section_background_color){
    $staff_section_background_style = "style='background:".$staff_section_background_color.";'";
} else {
    $staff_section_background_style = "";
}

// Set the CSS grid for this panel
$grid = "p-all m-1of2 t-1of3 d-1of3";
switch ($how_many_across) {
    case 1:
        $grid = "p-all m-all t-all d-all";
        break;
    case 2:
        $grid = "p-all m-1of2 t-1of2 d-1of2";
        break;
    case 3:
        $grid = "p-all m-1of2 t-1of3 d-1of3";
        break;
    case 4:
        $grid = "p-all m-1of2 t-1of4 d-1of4";
        break;
}

// tax_query set up
if( $staff_department != "" ) {
    $tax_query_args = array(
                        'taxonomy' => 'uwsp_faculty_departments',
                        'terms' => $staff_department
                        );
} else {
    // get all terms in the taxonomy
    $all_staff_terms = get_terms( 'uwsp_faculty_departments' );
    // convert array of term objects to array of term IDs
    $all_staff_terms_ids = wp_list_pluck( $all_staff_terms, 'term_id' );
    $tax_query_args = array(
                        'taxonomy' => 'uwsp_faculty_departments',
                        'terms' => $all_staff_terms_ids
                        );
}
// Set the $post to be a call for the faculty post type
$args = array(
        'post_type' => 'uwsp_faculty',
        'post_status' => 'publish',
        'orderby'   => 'rand',
        'numberposts' => $how_many_to_display,
        'tax_query' => array(
                            $tax_query_args
                            ),
    );

$staff_posts = get_posts($args);


$staff_selection_process = get_field("manual_or_auto_selection");
if($staff_selection_process == "staff_manual") :
    $staff_posts = get_field("manual_staff");
endif;

// set up intro grids
if( $intro_setting == 'yes_intro' && $intro_placement == 'left_intro' ) :
    $has_intro_grid_experts = "p-all m-all t-2of3 d-3of4";
    $has_intro_grid_intro = "p-all m-all t-1of3 d-1of4";
elseif ( $intro_setting == 'yes_intro' && $intro_placement == 'top_intro' ) : 
    $has_intro_grid_experts = "p-all m-all t-all d-all";
    $has_intro_grid_intro = "p-all m-all t-all d-all";
else :
    $has_intro_grid_experts = "";
    $has_intro_grid_intro = "";
endif;
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?> <?php echo "b-staff-panel_".$intro_placement; ?>" <?php echo $staff_section_background_style; ?>>
    <div class="b-posts-wrapper b-columns">
        <?php if( $intro_setting == 'yes_intro' ) : ?>
        <section class="b-staff-panel__intro  b-block-intro <?php echo $has_intro_grid_intro; ?>">
            <?php
            $see_all_url = get_post_type_archive_link('uwsp_faculty');
            $see_all_link = "<a class='b-latest-posts__intro-link' href='".$see_all_url."'>See All Faculty</a>";
            $description = get_field('experts_description', 'options');
            $template = array(
                array( 'core/heading', array(
                    'content' => 'Our Staff',
                    'level' => 2,
                    'align' => 'center',
                ) ),
                array( 'core/paragraph', array(
                    'content' => $see_all_link,
                    'align' => 'center',
                ) ),
                /*array( 'core/paragraph', array(
                    'content' => $description,
                    'align' => 'center',
                ) ),*/
            );
            echo '<InnerBlocks template="' . esc_attr( wp_json_encode( $template ) ) . '"  />';
            ?>

        </section>
        <?php endif; ?>
        <section class="b-staff-cards b-columns <?php echo $has_intro_grid_experts; ?> match-heights">
        <?php
        // Reference global $post variable.
        global $post;
        if( $staff_posts ) :
            foreach ($staff_posts as $post) :
                setup_postdata($post);
                echo "<div class='".$grid."'>";
                    get_template_part( 'template-parts/content-loop-staff' );
                echo "</div>";
            endforeach;
            wp_reset_postdata();
        endif;
        ?>
        <?php
            
        ?>
        </section>
    </div>
</div>
<?php
endif;
?>
