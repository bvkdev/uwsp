<?php

/**
 * Expert Panel Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'latest-posts-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'b-posts-panel';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assigning defaults.
$how_many_to_display = get_field('how_many_to_display') ?: 3;
$how_many_across = get_field('how_many_across') ?: 3;
$staff_department = get_field('staff_department') ?: '';
$intro_setting = get_field('include_an_intro') ?: 'yes_intro';
// Set Background Color
$posts_panel_background_color = get_field('background_color') ?: '#fff';
if($posts_panel_background_color){
    $posts_panel_background_style = "style='background:".$posts_panel_background_color.";'";
} else {
    $posts_panel_background_style = "";
}

// Set the CSS grid for this panel
$grid = "p-all m-1of2 t-1of3 d-1of3";
switch ($how_many_across) {
    case 1:
        $grid = "p-all m-all t-all d-all";
        break;
    case 2:
        $grid = "p-all m-all t-1of2 d-1of2";
        break;
    case 3:
        $grid = "p-all m-all t-1of3 d-1of3";
        break;
    case 4:
        $grid = "p-all m-all t-1of4 d-1of4";
        break;
}

// Check if we're on the home page.
// If so, add a meta query to the get_posts below
// this will only pull in posts that are allowed on the homepage
$home_query = "";
if( is_front_page() ) {
$home_query = array(
                array(
                    "key" => "allow_on_homepage",
                    "value" => true,
                    "compare" => "="
                )
            );
}

// Default intro values, used when there are no taxonomy-based limitations
$has_intro_title = "Latest News";
$see_all_url = get_permalink( get_option( 'page_for_posts' ) );
$see_all_link = "<a class='b-latest-posts__intro-link' href='".$see_all_url."'>See All Posts</a>";

// Start assigning limitations from the admin
$taxonomy_limitation = get_field('limit_by_taxonomy');
if( $taxonomy_limitation == 'limit_category' ) {

    $tax_group_terms = get_field('bb_categories');
    if($tax_group_terms != false) :
        if($tax_group_terms[0] != false) :
            $tax_group_terms_names = array();
            //only get the first term, in case someone has picked multiple categories
            $term_id = $tax_group_terms[0];
            $term_object = get_term_by('id', $term_id, 'category');
            if($term_object) :
                $term_nice = $term_object->name;
                array_push($tax_group_terms_names, $term_nice);
                $see_all_url = get_term_link($term_id);
                // $see_all_link = "<a class='b-latest-posts__intro-link' href='".$see_all_url."'>More Stories Like These</a>";
                $see_all_link = "<a class='b-latest-posts__intro-link' href='".$see_all_url."'>See More<span class='screen-reader-text'> ".$term_nice." Stories</span></a>";
                $has_intro_title = implode(" & ", $tax_group_terms_names);
            endif;
        endif;
    else :
        $all_terms = get_terms( array(
            'taxonomy' => 'category',
            'hide_empty' => true,
        ) );
        $tax_group_terms = wp_list_pluck($all_terms, 'term_id');
    endif;

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'orderby'   => 'date',
        'order'   => 'DESC',
        'numberposts' => $how_many_to_display,
        'tax_query' => array(
                        array(
                            'taxonomy' => 'category',
                            'terms' => $tax_group_terms,
                            )
                    ),
        'meta_query' => $home_query,
    );
// Are we dealing with the Tags taxonomy?
} elseif ( $taxonomy_limitation == 'limit_tag' ) {
    
    $tax_group_terms = get_field('bb_topics');

    if($tax_group_terms != false) :
        $tax_group_terms_names = array();
        //only get the first one
            $term_id = $tax_group_terms[0];
            $term_object = get_term_by('id', $term_id, 'post_tag');
            
            if($term_object) :
                $term_nice = $term_object->name;
                array_push($tax_group_terms_names, $term_nice);
                $see_all_url = get_term_link($term_id);
                $see_all_link = "<a class='b-latest-posts__intro-link' href='".$see_all_url."'>See More<span class='screen-reader-text'> ".$term_nice." Stories</span></a>";
                $has_intro_title = implode(" & ", $tax_group_terms_names);
            endif;
            
    else :
        $all_terms = get_terms( array(
            'taxonomy' => 'post_tag',
            'hide_empty' => true,
        ) );
        
        $tax_group_terms = wp_list_pluck($all_terms, 'term_id');

    endif;
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'orderby'   => 'date',
        'order'   => 'DESC',
        'numberposts' => $how_many_to_display,
        'tax_query' => array(
                        array(
                            'taxonomy' => 'post_tag',
                            'terms' => $tax_group_terms,
                            )
                    ),
        'meta_query' => $home_query,
    );
    //print_r($args);
} else {
    //$has_intro_title = "Latest Posts";
    //$see_all_url = get_permalink( get_option( 'page_for_posts' ) );
    //$see_all_link = "<a class='b-latest-posts__intro-link' href='".$see_all_url."'>See All Stories</a>";

    // Should we exclude categories?
    $excluded_posts = get_field('exclude_from_all_feeds', 'options');

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'orderby'   => 'date',
        'order'   => 'DESC',
        'numberposts' => $how_many_to_display,
        'category__not_in' => $excluded_posts,
        'meta_query' => $home_query,
    );
}
$latest_posts = get_posts($args);

// set up intro grids
if( $intro_setting == 'yes_intro' ) :
$has_intro_grid_experts = "p-all m-all t-2of3 d-3of4";

else :
    $has_intro_grid = "";
endif;

if ( count($latest_posts) ) :
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <?php
    if( $posts_panel_background_style != "" ) :
    ?>
    <span class="b-latest-posts__bkg" <?php echo $posts_panel_background_style; ?>></span>
    <?php
    endif;
    ?>
    <div class="b-posts-wrapper ">
        <?php if( $intro_setting == 'yes_intro' ) : ?>
        <section class="b-latest-posts__intro b-block-intro" <?php echo $posts_panel_background_style; ?>>
            <?php
            $template = array(
                array( 'core/heading', array(
                    'content' => $has_intro_title,
                    'level' => 2,
                    'align' => 'center',
                ) ),
                array( 'core/paragraph', array(
                    'content' => $see_all_link,
                    'align' => 'center',
                ) ),

            );
            echo '<InnerBlocks template="' . esc_attr( wp_json_encode( $template ) ) . '"  />';
            ?>

        </section>
        <?php endif; ?>
        <section class="b-columns">
        <?php
        // Reference global $post variable.
        global $post;
        if( $latest_posts ) :
            foreach ($latest_posts as $post) :
                setup_postdata($post);
                echo "<div class='".$grid."'>";
                    get_template_part( 'template-parts/content-loop' );
                echo "</div>";
            endforeach;
            wp_reset_postdata();
        endif;
        ?>
        <?php

        ?>
        </section>
    </div>
</div>

<?php endif; ?>
