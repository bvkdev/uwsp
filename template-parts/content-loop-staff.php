<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package getwid_base
 */

?>
<?php
$staff_terms = get_the_terms( $post->ID, 'uwsp_faculty_departments' );
$staff_first_term_id = $staff_terms[0]->term_id ?? 0;
$staff_term_image = get_field('feature_image', 'category_'.$staff_first_term_id);
$staff_header_style = "";
if( $staff_term_image != "" ) :
    $staff_header_style = "style='background: gray url(".$staff_term_image.") center center no-repeat; background-size: cover;'";
else :
    $staff_header_style = "";
endif;

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('b-staff-card'); ?>>

	<header class="entry-header" <?php echo $staff_header_style; ?> >
		<div class="content">
	        <?php getwid_base_post_thumbnail('thumbnail'); ?>
	    </div>
	</header><!-- .entry-header -->

	<div class="entry-content">
        <?php
        the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
        $staff_titles = get_field('titles', get_the_ID());
        //var_dump($staff_titles);
        echo '<p class="b-staff-card__meta b-staff-card__meta_titles">';
            if( have_rows('titles', get_the_ID() ) ) :
                while( have_rows('titles', get_the_ID() ) ): the_row();
                    echo "<span>".the_sub_field('title')."</span>";
                endwhile;
            endif;
        echo '</p>';
         // bb_terms_list('uwsp_faculty_departments', '<p class="b-staff-card__meta">', '</p>');
        ?>

		<?php
        if( has_excerpt() ) :
    		ob_start();
            the_excerpt( sprintf(
    			wp_kses(
    			/* translators: %s: Name of current post. Only visible to screen readers */
    				__( 'Continue reading about<span class="screen-reader-text"> "%s"</span>', 'getwid-base' ),
    				array(
    					'span' => array(
    						'class' => array(),
    					),
    				)
    			),
    			get_the_title()
    		) );
            $content = ob_get_contents();
            ob_end_clean();
            echo ($content);
        endif;
		?>

        <a href="<?php the_permalink(); ?>" class="entry-content-read-more" aria-hidden="true">Read More <span class="screen-reader-text">about <?php the_title(); ?></span></a>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
