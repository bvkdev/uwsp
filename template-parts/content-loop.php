<?php
/**
 * Template part for displaying posts
 *
 * Accepted arguments:
 * - $args['eyebrow'] | string
 * - $args['button']  | string
 * - $args['blurb']   | int
 */
$eyebrow = $args['eyebrow'] ?? false;
$button  = $args['button']  ?? false;
$blurb   = $args['blurb']   ?? false;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-in-loop'); ?>>

	<?php getwid_base_post_thumbnail('page-header-half-width'); ?>

	<header class="entry-header">
        <?php if ( $eyebrow ) : ?>
            <span class="entry-eyebrow"><?= $eyebrow ?></span>
        <?php endif; ?>
		<?php
		the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );

		if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php
				getwid_base_posted_on();
				getwid_base_edit_link();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>

        <?php if ( $blurb && strlen($post->post_content) ) : ?>
            <p class="entry-blurb"><?= wp_trim_words($post->post_content, $blurb) ?></p>
        <?php endif; ?>

        <?php if ( $button ) : ?>
            <button onclick="window.location.assign('<?= esc_url(get_permalink()) ?>')"><?= $args['button'] ?><i class="far fa-arrow-alt-circle-right"></i></button>
        <?php endif; ?>
	</header><!-- .entry-header -->
</article><!-- #post-<?php the_ID(); ?> -->
