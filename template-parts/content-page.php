<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package getwid_base
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(['d-2of3', 't-all', 'm-all', 'p-all']); ?>>
	<?php /*
    <header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        <?php getwid_base_post_thumbnail(); ?>
        <div class="uwsp-breadcrumbs"><a href="#"><i class="fal fa-home-alt"></i></a>/<a href="#">Academics</a>/<a href="#">Colleges</a>/<a href="#">College of Letters and Science</a>/<a href="#">School of Behavior & Social Sciences</a>/</div>
	</header><!-- .entry-header -->
    */ ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'getwid-base' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'getwid-base' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
