<?php
/**
 * Template part for displaying posts
 *
 * @package getwid_base
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(['d-2of3','t-all', 'p-all']); ?>>
	<header class="entry-header">
		<?php
        $cats = wp_get_post_categories(get_the_ID(), ['fields' => 'names']);
        if ( $cats ) :
        ?>
            <ul class="entry-header__cats">
                <?php
                echo implode('', array_map(function($str) {
                    return '<li>' . $str . '</li>';
                }, $cats));
                ?>
            </ul>
        <?php endif; ?>

        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

        <div class="entry-header__meta">
            <span class="entry-header__meta-date">
                <?php if ( ! in_array('Stories', $cats) ) : ?>
                    <?= get_the_date(get_option( 'date_format' ), $post); ?>
                <?php endif; ?>
            </span>
            <div class="entry-header__meta-social">
                <p class="b-share-button b-share-button_label">Share: </p>
                <ul>
                    <li><a class="b-share-button addthis_button_facebook at300b" title="Facebook" href="" aria-label="Share this post on Facebook"><i class="fab fa-facebook-square"></i></a></li>
                    <li><a class="b-share-button addthis_button_twitter at300b" title="Twitter" href="" aria-label="Share this post on Twitter"><i class="fab fa-twitter-square"></i></a></li>
                    <li><a class="b-share-button addthis_button_email at300b" title="Copy link" href="" aria-label="Copy link"><i class="fas fa-link"></i></a></li>
                </ul>
                <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51de2d2b7e09a8f5"></script>
            </div>
        </div>

        <?php getwid_base_post_thumbnail( 'getwid_base-large' ); ?>

        <?php /*
		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				getwid_base_posted_on();
				getwid_base_posted_by();
				getwid_base_posted_in();
				getwid_base_comments_link();
				getwid_base_edit_link();
				?>
			</div><!-- .entry-meta -->
		<?php endif; */ ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'getwid-base' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'getwid-base' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php getwid_base_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
