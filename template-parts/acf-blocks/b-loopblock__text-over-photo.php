<?php // CURRENT ROW LAYOUT = text_over_photo  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);

?>
<?php
$headline = get_sub_field('headline');
$subhead = get_sub_field('subheadline');
$intro = get_sub_field('intro');
$buttons = get_sub_field('buttons');
$font_size = get_sub_field('text_size');
$display_size = get_sub_field('display_size');
$no_text = "";
if( $headline == "" && $subhead == "" && $intro == "" && $buttons == "") {
    $no_text = "no_text";
}

$is_h1 = get_sub_field('make_h1');
?>



<section <?php echo $id_tag; ?>  class="b-section b-section_outer-pad b-section_textphoto b-section_textphoto_<?php echo $display_size; ?> <?php echo $no_text; ?> <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_textphoto">
		<div class="b-section__wrap-inner b-section__wrap-inner_textphoto" >
			<?php if($headline): ?>
				<?php if($is_h1): ?>
					<h1 class="h2 b-section-textphoto__title b-seo-title "><?php echo($headline); ?></h1>
				<?php else : ?>
					<h2 class="h2 b-section-textphoto__title b-seo-title "><?php echo($headline); ?></h2>
				<?php endif; ?>
			<?php endif; ?>
            <?php if($subhead): ?>
                <h3 class="h2 b-section__subtitle b-section-textphoto__subtitle <?php echo $inverse_class; ?>"><?php echo $subhead; ?></h3>
            <?php endif; ?>
			<?php if($intro): ?> <div class="b-wysiwyg b-section-textphoto__text"><?php echo $intro; ?></div><?php endif; ?>
			<?php if( have_rows('button') ) : ?>
                                <div class="b-textphoto__buttons b-buttons-group">
                                <?php while( have_rows('button') ): the_row(); ?>
                                    <?php
                                    $button_details = bbox_get_button_details();
                                    $button_label = $button_details["label"];
                                    $button_link = $button_details["link"];
                                    ?>
                                    <?php if($button_label && $button_link): ?>
                                        <a href=<?php echo $button_link; ?> class="b-buttons-group__button b-buttons-group__button_<?php echo get_row_index(); ?> b-textphoto__button b-textphoto__button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a>
                                    <?php endif;  // End check for single button label and link ?>
                                    
                                <?php endwhile; // End buttons loop?>
                                </div>
                            <?php endif; // End check for button repeater ?>
		</div>
	</div>
</section>
