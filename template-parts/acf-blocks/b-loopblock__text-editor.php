<?php // CURRENT ROW LAYOUT = text_editor  ?>
<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
// Get values for page intro
$intro_headline = get_sub_field('headline');
$intro_text = get_sub_field('intro');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_textedit b-textedit <?php echo $collapse_class; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_textedit cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_textedit cf">
			<?php if($intro_headline): ?> <h2 class="h2 b-headline b-textedit__headline"><?php echo $intro_headline; ?></h2><?php endif; ?>
			<?php if($intro_text): ?> <div class="b-textedit__text b-wysiwyg"><?php echo $intro_text; ?></div><?php endif; ?>
		</div>
	</div>
</section>