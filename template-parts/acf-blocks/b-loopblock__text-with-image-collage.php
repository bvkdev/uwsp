<?php // CURRENT ROW LAYOUT = text_with_image_collage ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
//
//$section_title = get_sub_field('section_title');
//$button_label = ro_get_button_label();
//$section_button_link = ro_button_link();
?>

<?php
$image_url = get_sub_field('image');
$headline = get_sub_field('headline');
$subheadline = get_sub_field('subheadline');
$intro = get_sub_field('intro');
$images = get_sub_field('images');
$background_size = "cover";
//
$is_h1 = get_sub_field('make_h1');
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?> b-section_text-image-collage" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_text-image-collage">
		<div class="b-section__wrap-inner b-section__wrap-inner_text-image-collage" >
            <div class="b-text-image-collage__content">
    		<?php if( have_rows('images') ): ?>
                <?php $image_num = 1; ?>
                <div class="b-collage-photo-main">
                    <?php while( have_rows('images') ): the_row();
                        $image_url = get_sub_field('image');
                    ?>
                    <?php if($image_num == 1): ?>
                        <div class="b-resp-image b-collage-photos_img_<?php echo $image_num; ?>">
                            <img src="<?php echo $image_url; ?>" />
                        </div>
                    <?php endif; ?>
                        <?php $image_num++; ?>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
			
            <div class="b-text-image-collage__text">
				<?php if($headline): ?>
					<?php if($is_h1): ?>
						<h1 class="b-image-collage__headline b-seo-title"><span><?php echo $headline; ?></span></h1>
					<?php else : ?>
						<h2 class="b-image-collage__headline b-seo-title"><span><?php echo $headline; ?></span></h2>
					<?php endif; ?>
				<?php endif; ?>
				<?php if($subheadline): ?><h3 class="h2 b-subhead b-image-collage__subheadline"><?php echo $subheadline; ?></h3><?php endif; ?>
				<?php if($intro): ?> <div class="b-wysiwyg b-image-collage__wysiwyg"><?php echo $intro; ?></div><?php endif; ?>
                <?php if( have_rows('button') ) : ?>
                <div class="b-image-collage__buttons b-buttons-group">
                    <?php while( have_rows('button') ): the_row(); ?>
                        <?php
                            $button_details = bbox_get_button_details();
                            $button_label = $button_details["label"];
                            $button_link = $button_details["link"];
                        ?>
                        <?php if($button_label && $button_link): ?>
                            <a href=<?php echo $button_link; ?> class="b-buttons-group__button b-buttons-group__button_<?php echo get_row_index(); ?> b-image-collage__button b-image-collage__button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a>
                        <?php endif;  // End check for single button label and link ?>
                        
                    <?php endwhile; // End buttons loop?>
    			</div>
    			<?php endif; // End check for button repeater ?>
			</div>
            </div><?php // End first photo and text container ?>
            <?php if( have_rows('images') ): ?>
                <?php $image_num = 1; ?>
                <div class="b-collage-photos">
                    <?php while( have_rows('images') ): the_row();
                        $image_url = get_sub_field('image');
                    ?>
                    <?php if($image_num == 1): ?>
                        
                    <?php else : ?>
                        <div class="b-resp-image b-collage-photos_img_<?php echo $image_num; ?>">
                            <img class="content" src="<?php echo $image_url; ?>" />
                        </div>
                        <?php endif; ?>
                        <?php $image_num++; ?>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>