<?php // CURRENT ROW LAYOUT = feature-cards  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$background_color = bb_get_background_color();
if($background_color == NULL) { $background_color = "#fff"; }
$set_the_color = "color: ".$background_color;
// we need to set the color of the entire section in order to inherit it into background gradients using the currentColor CSS setting
$section_bkg_style = substr( $section_bkg_style,0,-1 ). $set_the_color . substr( $section_bkg_style, -1 );
$inverse_class = ro_inverse_text();

$collapse_class = ro_collapse_padding($ro_prefix);
$section_title = get_sub_field('headline');
$section_subtitle = get_sub_field('subheadline');
$section_text = get_sub_field('intro');
?>
<?php
$section_style = "";
$layout_choice = get_sub_field('background_image_style');
if($layout_choice == 'option_2'):
	$section_style = '_photolead';
else :
	$section_style = '_default';
endif;
?>
<?php

$grid = "p-all m-1of2 t-1of3 d-1of3";
$row_amount = get_sub_field('how_many_across');
if(! $row_amount) {
    $row_amount = 3;
}
switch ($row_amount) {
    case 1:
        $grid = "p-all m-all t-all d-all";
        break;
    case 2:
        $grid = "p-all m-1of2 t-1of2 d-1of2";
        break;
    case 3:
        $grid = "p-all m-1of2 t-1of3 d-1of3";
        break;
	case 4:
        $grid = "p-all m-all t-1of4 d-1of4";
        break;
	case 5:
        $grid = "p-all m-all t-1of3 d-1of5";
        break;
	case 6:
        $grid = "p-all m-all t-1of3 d-1of6";
        break;
	case 7:
        $grid = "p-all m-all t-1of4 d-1of7";
        break;
	case 8:
        $grid = "p-all m-all t-1of4 d-1of8";
        break;
}
//
$hover_color = get_sub_field('hover_color');
if($hover_color){
	$hover_style = "style='background:".$hover_color.";'";
} else {
	$hover_style = "";
}
?>
<section <?php echo $id_tag; ?> class="b-section b-section__feature-cards b-section__feature-cards<?php echo $section_style; ?> <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?> " <?php if($layout_choice == "default") { echo $section_bkg_style; } ?>>
	<?php
    if($layout_choice == "option_2") {
        echo "<div class='b-feature-cards__photolead' ".$section_bkg_style."></div>";
    }
    ?>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_feature-cards cf">
		
		<div class="b-section__wrap-inner b-section__wrap-inner_feature-cards cf ">
            
		<?php if($section_title): ?>
			<h2 class="b-section__title b-seo-title b-feature-cards__title <?php echo $inverse_class; ?>"><span><?php echo $section_title; ?></span></h2>
		<?php endif; ?>
        <?php if($section_subtitle): ?>
            <h3 class="h2 b-section__subtitle b-feature-cards__subtitle <?php echo $inverse_class; ?>"><?php echo $section_subtitle; ?></h3>
        <?php endif; ?>
        <?php if($section_text): ?>
            <div class="b-wysiwyg b-section__text b-feature-cards__text <?php echo $inverse_class; ?>"><?php echo $section_text; ?></div>
        <?php endif; ?>
			
		<?php if( have_rows('feature_cards') ): ?>
		
	    <ul class="b-feature-cards b-columns">
	    <?php while( have_rows('feature_cards') ): the_row(); ?>
	    	<?php
			$image = get_sub_field('image');
			$headline = get_sub_field('headline');
			$text = get_sub_field('text');
			
			//$inverse_class = inverse_text();
			?>
			<li class="b-card b-feature-card <?php echo $grid; ?>">
				<div class="b-card__inner b-feature-card__inner b-feature-card__inner<?php echo $section_style; ?>">
						
						<?php if($image): ?>
                            <div class="b-card__resp-image b-feature-card__image b-resp-image">
								<img class="b-image_feature-card" alt="<?php echo $headline; ?>" src="<?php echo $image; ?>" />
							</div>
											
						<?php endif; ?>
													
						<div class="b-card__content b-feature-card__content">
							<?php if($headline): ?> <h3 class="b-card__headline b-feature-card__headline"><?php echo $headline; ?></h3><?php endif; ?>
							<?php if($text): ?> <div class="b-wysiwyg b-card__text b-feature-card__text"><?php echo $text; ?></div><?php endif; ?>
							
                            <?php if( have_rows('button') ) : ?>
                                <div class="b-card__buttons b-feature-card__buttons b-buttons-group">
                                <?php while( have_rows('button') ): the_row(); ?>
                                    <?php
                                    $button_details = bbox_get_button_details();
                                    $button_label = $button_details["label"];
                                    $button_link = $button_details["link"];
                                    ?>
                                    <?php if($button_label && $button_link): ?>
                                        <a href=<?php echo $button_link; ?> class="b-card__button b-buttons-group__button b-buttons-group__button_<?php echo get_row_index(); ?> b-feature-card__button b-feature-card__button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a>
                                    <?php endif;  // End check for single button label and link ?>
                                    
                                <?php endwhile; // End buttons loop?>
                                </div>
                            <?php endif; // End check for button repeater ?>
						</div>
				</div>
			</li>
		<?php endwhile; // End cards repeater loop?>
		<?php
        /*
		$cards_amount = count(get_sub_field('feature_cards'));
        //echo $cards_amount;
        //echo '<br />';
        //echo $row_amount;
        $last_row_spots = $cards_amount % $row_amount;
        $missing_spots = $row_amount - $last_row_spots;
        //echo $missing_spots;
        for ($i=0; $i < $missing_spots; $i++) { 
            echo '<li class="b-feature-card'.$grid.'"></li>';
        }
        */
        ?>
	    </ul>
		<?php endif; ?>
	</div></div>
</section>
