<?php
				// check if the flexible content field has rows of data
				if( have_rows('content_blocks') ):
				
				 	// loop through the rows of data
				    while ( have_rows('content_blocks') ) : the_row();
?>				
						
                        <?php // current row layout = EMBEDS AND SCRIPTS ?>     
                        <?php if( get_row_layout() == 'embeds_and_scripts' ): ?>
                            <?php get_template_part('template-parts/acf-blocks/b-loopblock__embeds-and-scripts'); ?>

						<?php // current row layout = FEATURE CARDS ?>
						<?php elseif( get_row_layout() == 'feature_cards' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__feature-cards'); ?>

						
                        <?php // current row layout = FUNNELING BUTTONS ?>     
                        <?php elseif( get_row_layout() == 'funneling_buttons' ): ?>
                            <?php get_template_part('template-parts/acf-blocks/b-loopblock__funneling'); ?>
						
						
						<?php // current row layout = HERO SLIDE SHOW ?>		
						<?php elseif( get_row_layout() == 'page_hero' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__page-hero'); ?>
						
						<?php // current row layout = HORIZONTAL RULE ?>		
						<?php elseif( get_row_layout() == 'horizontal_rule' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__horizontal-rule'); ?>						
                        
                        
                        <?php // current row layout = IMAGE PARAGRAPHS ?>     
                        <?php elseif( get_row_layout() == 'image_paragraphs' ): ?>
                            <?php get_template_part('template-parts/acf-blocks/b-loopblock__image-paragraphs'); ?>
					
						
						<?php // current row layout = POST TYPE ARCHIVE ?>		
						<?php elseif( get_row_layout() == 'post_type_archive' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__post-type-archive'); ?>
                        

                        <?php // current row layout = QUICK LINKS ?>     
                        <?php elseif( get_row_layout() == 'quick_links' ): ?>
                            <?php get_template_part('template-parts/acf-blocks/b-loopblock__quicklinks'); ?>
						
						
						<?php // current row layout = QUOTE CAROUSEL ?>		
						<?php elseif( get_row_layout() == 'quote_carousel' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__quote-carousel'); ?>
								
						
						<?php // current row layout = STATISTICS ?>		
						<?php elseif( get_row_layout() == 'statistics' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__statistics'); ?>
												
							
						<?php // current row layout = TEXT INTRO ?>		
						<?php elseif( get_row_layout() == 'text_intro' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__text-intro'); ?>
						
						
						<?php // current row layout = TEXT EDIT ?>		
						<?php elseif( get_row_layout() == 'text_editor' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__text-editor'); ?>
							
						
						<?php // current row layout = TEXT OVER PHOTO ?>		
						<?php elseif( get_row_layout() == 'text_over_photo' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__text-over-photo'); ?>
							
							
						<?php // current row layout = TEXT AREA WITH IMAGE COLLAGE ?>
						<?php elseif( get_row_layout() == 'text_with_image_collage' ): ?>
							<?php get_template_part('template-parts/acf-blocks/b-loopblock__text-with-image-collage'); ?>
                            
                            
                        <?php // current row layout = TEXT AREA WITH SIDE IMAGE ?>
                        <?php elseif( get_row_layout() == 'text_with_side_image' ): ?>
                            <?php get_template_part('template-parts/acf-blocks/b-loopblock__text-with-side-image'); ?>
						
						
						<?php  
						endif; // END current row layout check
				
				    endwhile;
				
				else :
				
				    // no layouts found
				
				endif;
				
				?>