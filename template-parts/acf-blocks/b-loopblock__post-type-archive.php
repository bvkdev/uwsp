<?php // CURRENT ROW LAYOUT = POST TYPE ARCHIVE  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$background_color = bb_get_background_color();
if($background_color == NULL) { $background_color = "#fff"; }
$set_the_color = "color: ".$background_color;
// we need to set the color of the entire section in order to inherit it into background gradients using the currentColor CSS setting
$section_bkg_style = substr( $section_bkg_style,0,-1 ). $set_the_color . substr( $section_bkg_style, -1 );
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
$section_title = get_sub_field('section_title');
?>
<?php
$intro_headline = get_sub_field('headline');
$button_details = bbox_get_button_details();
$button_label = $button_details["label"];
$button_link = $button_details["link"];
?>
<?php
// get the value of the layout option
$display_type = get_sub_field('layout_style') ?: 'cards';

// set up the grid for the card-based layout
$grid = "p-all m-all t-1of3 d-1of3";
$row_amount = get_sub_field('how_many_across');
if($row_amount == "") {
    $row_amount = 3;
}
switch ($row_amount) {
    case 1:
        $grid = "p-all m-1of2 t-1of2 d-1of3";
        break;
    case 2:
        $grid = "p-all m-1of2 t-1of2 d-1of2";
        break;
    case 3:
        $grid = "p-all m-1of2 t-1of3 d-1of3";
        break;
	case 4:
        $grid = "p-all m-1of2 t-1of2 d-1of4";
        break;
	case 5:
        $grid = "p-all m-1of2 t-1of3 d-1of5";
        break;
	case 6:
        $grid = "p-all m-1of2 t-1of3 d-1of6";
        break;
	case 7:
        $grid = "p-all m-1of2 t-1of4 d-1of7";
        break;
	case 8:
        $grid = "p-all m-1of2 t-1of2 d-1of8";
        break;
}
if( $display_type == 'news_feed' ) {
    $grid = "";
}
//echo "grid is ".$grid;

//not used right now. Retaining for possible future use.
$is_h1 = get_sub_field('make_h1');
?>

<?php
//GET POSTS BASED ON WHAT INCLUSION METHOD TO USE: AUTO OR MANUAL SELECTION
$selection_method = get_sub_field('selection_method');
if( $selection_method == "") {
	$selection_method = "automatic";
}
//
$type = get_sub_field('post_type_selection');
// This field was removed, but could be added back to allow for order selection.
$order = get_sub_field('order_of_appearance');
if(!$order):
	$order = "DSC";
endif;
// needed to normalize the naming of the post type declaration to something more human friendly (in some cases)
if($type == 'page') {
	$type_variable = 'page';	
} elseif($type == 'event_type') {
	$type_variable = 'event';
} elseif ($type == 'post') {
    $type_variable = 'post';
} else {
    // for 'post as a default option'
	$type_variable = 'post';
    $type = 'post';
}

if( $selection_method == "manual") {
		
	$featured_post_data = get_sub_field('featured_selections', false, false);
	$featured_post_data_amount = count($featured_post_data);	
	$args=array(
		'post_type' => $type,
		'post_status' => 'publish',
		//'posts_per_page' => $featured_post_data_amount,
		'post__in'	=> $featured_post_data,
		'ignore_sticky_posts'=> 0,
		'orderby' =>  'post__in'
	);
	
		
} elseif( $selection_method == "automatic") {
    $number_of_auto_posts = get_sub_field("how_many_to_display") ?: 6;
	if($type == 'page') {
        $type_variable = 'page';
        $args=array(
            'post_type' => $type,
            'post_status' => 'publish',
            'posts_per_page' => $number_of_auto_posts,
            'ignore_sticky_posts'=> 1,
            'order'=>$order
        );
    } elseif($type == 'event_type') {
		$type_variable = 'event';
		$args=array(
			'post_type' => $type,
			'post_status' => 'publish',
			'posts_per_page' => $number_of_auto_posts,
			'ignore_sticky_posts'=> 1,
			'meta_key' => 'start_date',
			'orderby' => 'meta_value',
			'type' => 'DATE',
			'order' =>$order,
		);
	} elseif($type == 'post') {
		$type_variable = 'post';
		$args=array(
			'post_type' => $type,
			'post_status' => 'publish',
			'posts_per_page' => $number_of_auto_posts,
			'ignore_sticky_posts'=> 1,
			'order'=>$order
		);
	}
}
$my_query = null;
$my_query = new WP_Query($args);

if( $my_query->have_posts() ) : ?>
						
<section <?php echo $id_tag; ?> class="b-section b-section_posts-archive b-section_posts-archive_<?php echo $type_variable; ?> b-section_posts-archive_<?php echo $display_type; ?>-layout  b-section_outer-pad <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?>" <?php if($display_type != "news_feed") { echo $section_bkg_style; } ?>>
	<?php
    if($display_type == "news_feed") {
        echo "<div class='b-section_posts-archives__photolead' ".$section_bkg_style."></div>";
    }
    ?>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_posts-archive">
		
		<div class="b-section__wrap-inner b-section__wrap-inner_posts-archive">	
		
            <?php if($intro_headline): ?>
                <?php if($is_h1): ?>
                    <h1 class="h2 b-section_posts-archive__headline <?php echo $inverse_class; ?>"><?php echo $intro_headline; ?></h1>
                <?php else : ?>
                    <h2 class="h2 b-section_posts-archive__headline <?php echo $inverse_class; ?>"><?php echo $intro_headline; ?></h2>
                <?php endif; ?>
            <?php endif; ?>
            <?php if( have_rows('button') ) : ?>
                <div class="b-section_posts-archive__buttons b-buttons-group <?php echo $inverse_class; ?>">
                <?php while( have_rows('button') ): the_row(); ?>
                    <?php
                        $button_details = bbox_get_button_details();
                        $button_label = $button_details["label"];
                        $button_link = $button_details["link"];
                    ?>
                    <?php if($button_label && $button_link): ?>
                        <a href=<?php echo $button_link; ?> class="b-buttons-group__button  b-section_posts-archive__button b-section_posts-archive__button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a>
                    <?php endif;  // End check for single button label and link ?>
                    
                <?php endwhile; // End buttons loop?>
                </div>
            <?php endif; // End check for button repeater ?>

	    <div class="b-posts-archive b-posts-archive_<?php echo $type_variable; ?> b-posts-archive_<?php echo $display_type; ?>-layout <?php /*if($display_type == 'news_feed'){ echo $inverse_class; }*/ ?>">
        <?php $post_index = 0; ?>
        <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
		<?php $post_index++; ?>
		<?php // REGULAR POSTS LOOP // ?>
        <?php if($type == 'post') : ?>

            <?php if( $display_type == 'news_feed' && $post_index <= 6  ) :
                    include(locate_template('template-parts/acf-blocks/display-snippets/b-single__post.php'));
                elseif ($display_type == 'cards' ) :
                    include(locate_template('template-parts/acf-blocks/display-snippets/b-single__post.php'));
                endif;
            ?>
		
		<?php // PAGE LOOP // ?>
		<?php elseif($type == 'page') : ?>
            <?php if( $display_type == 'news_feed' && $post_index <= 6  ) :
                    include(locate_template('template-parts/acf-blocks/display-snippets/b-single__page.php'));
                elseif ($display_type == 'cards' ) :
                    include(locate_template('template-parts/acf-blocks/display-snippets/b-single__page.php'));
               endif;
		    ?>
			
		<?php // EVENTS LOOP // ?>
		<?php elseif($type == 'event_type') : ?>
		    <?php include(locate_template('template-parts/acf-blocks/display-snippets/b-single__event.php')); ?>
			
		<?php // ACADEMIC PROGRAM LOOP - // ?>
        <?php // This post type is not set up yet - // ?>
		<?php elseif($type == 'academic_program_type') : ?>
		    <?php include(locate_template('template-parts/acf-blocks/display-snippets/b-single__academic-program.php')); ?>
			
		<?php endif; ?>	
	<?php endwhile; ?>
	
	<?php
        // This will add non-visual blocks to the grid to make up for flex layout's inability to display a centered grid without the last row being stuck in the middle
        if($display_type != "news_feed") {
            $cards_amount = $number_of_auto_posts;
            //echo "cards_amount is ".$cards_amount;
            //echo '<br />';
            //echo "row_amount is ".$row_amount;
            $last_row_spots = $cards_amount % $row_amount;
            //echo '<br />';
            //echo "last_row_spots is ".$last_row_spots;
            
            if ($last_row_spots != 0) {
                $missing_spots = $row_amount - $last_row_spots;
                //echo '<br />';
                //echo "missing_spots is ".$missing_spots;
                for ($i=0; $i < $missing_spots; $i++) { 
                    echo '<div class="b-grid-fixer '.$grid.'"></div>';
                }
            }
        }
    ?>		
	    </div>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	</div></div>
</section>
<?php endif; ?>	