<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
//
$script_field = get_sub_field('shortcode_field');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_shortcode <?php echo $collapse_class; ?> <?php echo $inverse_class; ?>  <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_shortcode cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_shortcode cf">
			<?php if($script_field): ?> <div class="b-script-holder b-script-holder__shortcode js-script-holder"><?php echo do_shortcode($script_field); ?></div><?php endif; ?>
		</div>
</section>