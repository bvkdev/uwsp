<?php // CURRENT ROW LAYOUT = side-image-text-area  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
//
$section_title = get_sub_field('section_title');
$button_label = ro_get_button_label();
$section_button_link = ro_button_link();
?>

<?php
$funnel_buttons = get_sub_field('funneling_button');
$include_supporting_text = get_sub_field('include_supporting_text');
$text_align = get_sub_field('text_align'); //defaults to right
$text_side = "b-text-side_".$text_align;

// Set up grid for funneling buttons
$grid_width = get_sub_field('button_grid_columns');
$grid = "";
if($grid_width == ""):
    $grid_width = 1;
    $grid = "p-all p-all t-all d-all";
elseif($grid_width == 1):
    $grid = "p-all p-all t-all d-all";
elseif($grid_width>1 && $grid_width <= 4):
    $grid = "p-1of2 m-1of2 t-1of".$grid_width." d-1of".$grid_width;
elseif($grid_width == 5 || $grid_width == 6):
    $grid = "p-1of2 m-1of2 t-1of".$grid_width." d-1of".$grid_width;
elseif($grid_width == 7 || $grid_width == 8):
    $grid = "p-1of2 m-1of4 t-1of4 d-1of".$grid_width;
endif; 

if($include_supporting_text == "no") {
    $text_align = "";
    $text_side = "";
    $image_side = "";
    $text_column_classes = "";
    $image_column_classes = "";
} else {
    // get the text values
    $clone_var = get_sub_field( 'text_inputs' );
    $headline = $clone_var['headline'];
    $subheadline = $clone_var['subheadline'];
    $intro = $clone_var['intro'];
    $intro = $clone_var['intro'];
    $text_column_classes = "p-all m-all t-2of5 d-2of5";
    $image_column_classes = "p-all m-all t-3of5 d-3of5";
}


//
$is_h1 = get_sub_field('make_h1');
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?> b-section_funneling" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_funneling cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_funneling cf " >
		
		<div class="b-funneling__content <?php if($include_supporting_text == "yes") : ?>b-columns<?php endif; ?>">
			
		    <?php if( have_rows('funneling_button') ) : ?>
            <div class="b-funneling__buttons <?php echo $image_column_classes; ?> <?php if($include_supporting_text == "no") : ?>b-funneling__buttons_large<?php endif; ?>">
                <?php while( have_rows('funneling_button') ): the_row(); ?>
                    <?php
                        $button_details = bbox_get_button_details();
                        $button_label = $button_details["label"];
                        $button_link = $button_details["link"];
                        $button_image = $button_details["image"];
                        if($button_image == "") :
                            $button_image = get_template_directory_uri()."/library/images/placeholder_buttons.jpg";
                        endif;
                        $button_style = "style='background: url(".$button_image.") center center no-repeat rgba(73, 47, 146, 0.8); background-size: cover; background-blend-mode: soft-light;'";

                    ?>
                    <?php if($button_label && $button_link): ?>
                        <a href=<?php echo $button_link; ?> class="b-funneling__buttons__button <?php echo $grid; ?>" <?php echo $button_style; ?>><?php echo $button_label; ?></a>
                    <?php endif;  // End check for single button label and link ?>
                    
                <?php endwhile; // End buttons loop?>
                <?php
                $cards_amount = count($funnel_buttons);
                //echo "cards_amount is ".$cards_amount;
                //echo '<br />';
                //echo "row_amount is ".$row_amount;
                $last_row_spots = $cards_amount % $grid_width;
                //echo '<br />';
                //echo "last_row_spots is ".$last_row_spots;
                
                if ($last_row_spots != 0) {
                    $missing_spots = $grid_width - $last_row_spots;
                    //echo '<br />';
                    //echo "missing_spots is ".$missing_spots;
                    for ($i=0; $i < $missing_spots; $i++) { 
                        echo '<span class="b-funneling__buttons__button b-grid-fixer '.$grid.'"></span>';
                    }
                }
                ?>
            </div>
            <?php endif; // End check for funneling button repeater ?>
			
            <?php if($include_supporting_text == "yes") : ?>
			<div class="b-funneling__text-side <?php echo $text_side; ?> <?php echo $text_column_classes; ?> ">
				<?php if($headline): ?>
					<?php if($is_h1): ?>
						<h1 class="b-funneling__headline b-seo-title"><span><?php echo $headline; ?></span></h1>
					<?php else : ?>
						<h2 class="b-funneling__headline b-seo-title"><span><?php echo $headline; ?></span></h2>
					<?php endif; ?>
				<?php endif; ?>
				<?php if($subheadline): ?><h3 class="h2 b-funneling__subheadline b-section__subtitle"><?php echo $subheadline; ?></h3><?php endif; ?>
				<?php if($intro): ?> <div class="b-wysiwyg b-funneling__text"><?php echo $intro; ?></div><?php endif; ?>
                <?php if( have_rows('button') ) : ?>
                <div class="b-funneling__text-buttons b-buttons-group">
                    <?php while( have_rows('button') ): the_row(); ?>
                        <?php
                            $button_details = bbox_get_button_details();
                            $button_label = $button_details["label"];
                            $button_link = $button_details["link"];
                        ?>
                        <?php if($button_label && $button_link): ?>
                            <a href=<?php echo $button_link; ?> class="b-buttons-group__button b-buttons-group__button_<?php echo get_row_index(); ?> b-funneling__text-button b-funneling__text-button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a>
                        <?php endif;  // End check for single button label and link ?>
                        
                    <?php endwhile; // End buttons loop?>
    			</div>
    			<?php endif; // End check for button repeater ?>
		</div> 
        <?php endif; // End check for should include supporting text?>
	</div></div>
</section>