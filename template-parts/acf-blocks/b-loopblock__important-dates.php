<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = ro_inverse_text();
// Get values for page intro
$intro_headline = get_sub_field('headline');
$is_h1 = get_sub_field('make_h1');
//
$repeated_items = get_sub_field("imp_date");
$grid = "p-all m-all t-1of3 d-1of3";
$row_amount = get_sub_field('grid_width');
switch ($row_amount) {
    case 1:
        $grid = "p-all m-all t-all d-all";
        break;
    case 2:
        $grid = "p-all m-all t-1of2 d-1of2";
        break;
    case 3:
        $grid = "p-all m-all t-1of3 d-1of3";
        break;
    case 4:
        $grid = "p-all m-all t-1of2 d-1of4";
        break;
    case 5:
        $grid = "p-all m-all t-1of3 d-1of5";
        break;
    case 6:
        $grid = "p-all m-all t-1of3 d-1of6";
        break;
    case 7:
        $grid = "p-all m-all t-1of4 d-1of7";
        break;
    case 8:
        $grid = "p-all m-all t-1of4 d-1of8";
        break;
}
?>

<section <?php echo $id_tag; ?> class="b-section b-section_imp-dates <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_imp-dates cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_imp-dates cf">
			<?php // SECTION INTRO CONTENT ?>
            <?php if($intro_headline): ?>
				<?php if($is_h1): ?>
					<h1 class="h2 b-section_imp-dates__headline"><?php echo $intro_headline; ?></h1>
				<?php else : ?>
					<h2 class="h2 b-section_imp-dates__headline"><?php echo $intro_headline; ?></h2>
				<?php endif; ?>
			<?php endif; ?>
            <?php if( have_rows('button') ) : ?>
                <div class="b-section_imp-dates__buttons b-buttons-group">
                <?php while( have_rows('button') ): the_row(); ?>
                    <?php
                        $button_details = bbox_get_button_details();
                        $button_label = $button_details["label"];
                        $button_link = $button_details["link"];
                    ?>
                    <?php if($button_label && $button_link): ?>
                        <a href=<?php echo $button_link; ?> class="b-buttons-group__button  b-section_imp-dates__button b-section_imp-dates__button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a>
                    <?php endif;  // End check for single button label and link ?>
                    
                <?php endwhile; // End buttons loop?>
                </div>
            <?php endif; // End check for button repeater ?>

            <?php // IMPORTANT DATES LOOP ?>
            <?php if( have_rows('imp_date') ) : ?>
                <ul class="b-imp-dates">
                <?php while( have_rows('imp_date') ): the_row(); ?>
                    <?php
                        $date_value = get_sub_field("date_value");
                        $date_label = get_sub_field("description");
                    ?>
                    <li class="b-imp-date <?php echo $grid; ?>">
                        <span class="b-imp-date__value"><?php echo $date_value; ?></span>
                        <div class="b-imp-date__content">
                            <span class="b-imp-date__description"><?php echo $date_label; ?></span>
                            <?php
                                $button_details = bbox_get_button_details();
                                //var_dump($button_details);
                                if($button_details["link"] != false) :
                            ?>
                                <div class="b-imp-date__buttons b-buttons-group">
                                    <?php
                                    
                                    $button_label = $button_details["label"];
                                    $button_link = $button_details["link"];
                                    ?>
                                    <?php if($button_label && $button_link): ?>
                                        <a href=<?php echo $button_link; ?> class="b-buttons-group__button b-imp-date__button"><?php echo $button_label; ?></a>
                                    <?php endif;  // End check for single button label and link ?>
                                </div>
                            <?php endif;  // End check for button value?>
                        </div>
                    </li>
                <?php endwhile; // End dates loop?>
                <?php
                $repeated_amount = count($repeated_items);
                //echo "cards_amount is ".$repeated_amount;
                //echo '<br />';
                //echo "row_amount is ".$row_amount;
                $last_row_spots = $repeated_amount % $row_amount;
                //echo '<br />';
                //echo "last_row_spots is ".$last_row_spots;
                
                if ($last_row_spots != 0) {
                    $missing_spots = $row_amount - $last_row_spots;
                    //echo '<br />';
                    //echo "missing_spots is ".$missing_spots;
                    for ($i=0; $i < $missing_spots; $i++) { 
                        echo '<li class="b-imp-date b-grid-fixer '.$grid.'"></li>';
                    }
                }
                ?>
                </ul>
            <?php endif; // End check for dates repeater ?>
		</div>
	</div>
</section>