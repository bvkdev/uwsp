<?php // CURRENT ROW LAYOUT = HALF PHOTO REPEATER  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
$section_button_link = ro_button_link();
$button_label = get_sub_field('button_text');
?>


<?php
//$grid = "";
//$section_title = get_sub_field('section_title');
$section_title = "";
// Set up which side the image should be on;
$image_align = get_sub_field('image_align') ?: 'left';
$image_side = "b-image-para__image_".$image_align;

// Set up the vertical alignment
$column_vertical = "";
$image_align_vertical = get_sub_field('vertical_align') ?: 'top';
$column_vertical = "b-columns_".$image_align_vertical;
?>
<section <?php echo $id_tag; ?> class="b-section b-section_image-para <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_image-para cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_image-para cf">
			<?php if($section_title): ?>
				<h2 class="b-headline h2 b-section__title"><span><?php echo $section_title; ?></span></h2>
			<?php endif; ?>	
			
		    <?php $tile_count = 1; ?>
		    	<?php
				$image = get_sub_field('image');
				$image_url = $image["url"];
				$headline = get_sub_field('headline');
				$subhead = get_sub_field('subheadline');
				$text = get_sub_field('intro');
				$button_label = ro_get_button_label();
				$button_link = ro_button_link();
				//H1
                $is_h1 = get_sub_field('make_h1');
                $myHtag = "h2";
                if($is_h1):
                    $myHtag = "h1";
                endif;
				?>
				<div class="b-columns <?php echo $column_vertical; ?>"> 
					<div class="b-image-para__image <?php echo $image_side; ?>">
						<?php if($image): ?> <img class="" src="<?php echo $image['url']; ?>" /><?php endif; ?>
					</div>
					<div class="b-image-para__content">
						<?php if($headline): ?> <<?php echo $myHtag;?> class="h2 b-image-para__headline"><?php echo $headline; ?></<?php echo $myHtag;?>><?php endif; ?>
						<?php if($subhead): ?> <p class="b-subhead b-image-para__subhead"><?php echo $subhead; ?></p><?php endif; ?>
						<?php if($text): ?> <div class="b-wysiwyg b-image-para__text"><?php echo $text; ?></div><?php endif; ?>
					</div>
					
				</div>


		</div>
	</div>
</section>