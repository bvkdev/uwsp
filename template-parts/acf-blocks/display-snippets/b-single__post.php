<?php
$post_date = get_the_date('m-j-y');
$post_title = get_the_title();
$post_author = get_the_author();
$post_excerpt = get_the_excerpt();
$post_id = get_the_ID();
$post_url = get_the_permalink();
//$post_image = get_the_post_thumbnail( $post_id, 'bones-thumb-700', array( 'class' => 'b-featured-post__thumbnail', 'alt' => $post_title ) );
$read_more_label = "Read More";
//$grid COULD BE COMING FROM THE POST ARCHIVE BLOCK, WHERE IT IS SET IN THE ADMIN.
//IN CASE THIS FILE IS BEING USED ELSEWHERE, LOOK FOR AND THEN DEFINE $GRID.
if (empty($grid)) {
    $grid = "p-all m-all t-1of3 d-1of3";
    $grid = "";
}
?>
<article class="b-post b-post_<?php echo $post_index; ?> b-card <?php echo $grid; ?>">
    
    <div class="b-post__inner b-card_inner js-equal-heights_rows">
        <div class="b-resp-image b-card__resp-image b-resp-image_post">
            <?php getwid_base_post_thumbnail(); ?>
        </div>
        <div class="b-post__content b-card__content">
            <?php if($post_title): ?><h3 class="h4 b-post__headline b-card__headline"><a class="b-post__headline-link" href="<?php echo $post_url; ?>"><?php echo $post_title; ?></a></h3><?php endif; ?>
            <p class="b-post__meta b-card__meta"><?php echo $post_date; ?></p>
            <?php if($post_excerpt): ?> <p class="b-wysiwyg b-post__text b-card__text"><?php echo $post_excerpt; ?></p><?php endif; ?>
            <a class="b-post__button b-card__button" href="<?php echo $post_url; ?>"><?php echo $read_more_label; ?></a>
        </div>
    </div>
</article>
                        
