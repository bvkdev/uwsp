<?php
$post_date = get_the_date('m-j-y');
$post_title = get_the_title();
$post_author = get_the_author();
$post_excerpt = get_the_excerpt();
//$post_excerpt = excerpt(150);
$post_id = get_the_ID();
$post_url = get_the_permalink();
//$post_image = getwid_base_post_thumbnail();
$read_more_label = "Listen In";
//$grid COULD BE COMING FROM THE POST ARCHIVE BLOCK, WHERE IT IS SET IN THE ADMIN.
//IN CASE THIS FILE IS BEING USED ELSEWHERE, LOOK FOR AND THEN DEFINE $GRID.
if (empty($grid)) {
    $grid = "p-all m-all t-1of3 d-1of3";
}
?>
<div class="b-card b-page-preview <?php echo $grid; ?>">
    
        <div class="b-post__inner b-card__inner b-page-preview__inner">
            <div class="b-resp-image b-card__resp-image b-page-preview__resp-image">
                <?php getwid_base_post_thumbnail(); ?>
            </div>
            <div class="b-page-preview__content b-card__content">
                <?php if($post_title): ?><h3 class="b-page-preview__name b-card__headline"><?php echo $post_title; ?></h3><?php endif; ?>
                <?php /*<?php if($post_excerpt): ?><p class="b-page-preview__text"><?php echo $post_excerpt; ?></p><?php endif; ?> */ ?>
                <a class="b-page-preview__button b-card__button" href="<?php echo $post_url; ?>">Learn More</a>
            </div>
        </div>
    
</div>