<?php // CURRENT ROW LAYOUT = side-image-text-area  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
//
$section_title = get_sub_field('section_title');
$button_label = ro_get_button_label();
$section_button_link = ro_button_link();
?>

<?php
$image_url = get_sub_field('image');
$video_oembed = get_sub_field('video');
$image_align = get_sub_field('image_align'); //defaults to right
$text_side = 'b-text-side_left';
$image_side = "b-image-side_".$image_align;
if( $image_align == "left" ) :
	$text_side = "b-text-side_right";
endif;
//
$text_column_classes = "p-all m-all t-2of5 d-2of5";
$image_column_classes = "p-all m-all t-3of5 d-3of5";
$content_weight = "image-heavy";
$favor_text = get_sub_field('favor_the_text');
if($favor_text) :
	$text_column_classes = "p-all m-all t-3of5 d-3of5";
	$image_column_classes = "p-all m-all t-2of5 d-2of5";
	$content_weight = "text-heavy";
endif;
//
$headline = get_sub_field('headline');
$subheadline = get_sub_field('subheadline');
$intro = get_sub_field('intro');
$images = get_sub_field('images');
if($images != ""):
    $image_count = sizeof($images);
else :
    $image_count = 0;
endif;
if( $image_count == 1 ) {
	$background_size = "cover";
} else {
	$background_size = "cover";
}
$column_amount = 2;
//
$is_h1 = get_sub_field('make_h1');
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?> b-section_text-side-image" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_text-side-image cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_text-side-image cf " >
		
		<div class="b-text-side-image__content b-columns">
			<?php if ($video_oembed != "") : ?>
                <div class="b-side-video  <?php echo $image_side; ?> <?php echo $image_column_classes; ?>  <?php echo $content_weight; ?>">
                    <div class="embed-container">
                    <?php echo $video_oembed; ?>
                    </div>
                </div>
			<?php elseif( have_rows('images') ): ?>
				<?php $image_num = 1; ?>
			<div class="b-side-photos b-side-photos_<?php echo $image_count; ?> <?php echo $image_side; ?> <?php echo $image_column_classes; ?> <?php echo $content_weight; ?>">
			    <?php while( have_rows('images') ): the_row();
			    	$image_url = get_sub_field('image');
					$background_style = "style='background: url(".$image_url.") center center no-repeat; background-size:".$background_size.";'";
			 	?>
			 		<div class="b-resp-image b-side-photo" <?php echo $background_style; ?>>
			    	</div>
			    	<?php $image_num++; ?>
			    <?php endwhile; ?>
			</div>
			<?php endif; ?>
			
			<div class="b-side-text <?php echo $text_side; ?> <?php echo $text_column_classes; ?> <?php echo $content_weight; ?>">
				<?php if($headline): ?>
					<?php if($is_h1): ?>
						<h1 class="b-side-text__headline b-seo-title"><span><?php echo $headline; ?></span></h1>
					<?php else : ?>
						<h2 class="b-side-text__headline b-seo-title"><span><?php echo $headline; ?></span></h2>
					<?php endif; ?>
				<?php endif; ?>
				<?php if($subheadline): ?><h3 class="h2 b-subhead b-side-text__subheadline"><?php echo $subheadline; ?></h3><?php endif; ?>
				<?php if($intro): ?> <div class="b-wysiwyg b-side-text__text"><?php echo $intro; ?></div><?php endif; ?>
				<?php $btnCount = 0; ?>
				<?php $rowCount = 0; ?>
                <?php if( have_rows('button') ) : ?>
                	<?php while( have_rows('button') ): the_row(); $btnCount++;  endwhile;?>
	                <div class="b-side-text__buttons b-buttons-group <?php if($btnCount > 2) { ?>split<?php } ?>">
	                    
	                    <?php while( have_rows('button') ): the_row(); $rowCount++;   ?>
	                        <?php
	                            $button_details = bbox_get_button_details();
	                            $button_label = $button_details["label"];
	                            $button_link = $button_details["link"];
	                            $button_image = $button_details["image"];
		                        if($button_image == "") :
		                            $button_image = get_template_directory_uri()."/library/images/placeholder_buttons.jpg";
		                        endif;
		                        $button_style = "style='background: url(".$button_image.") center center no-repeat rgba(73, 47, 146, 0.8); background-size: cover; background-blend-mode: soft-light;'";
	                        ?>
	                        <?php if($button_label && $button_link && $btnCount < 3 || $rowCount == 1): ?>
	                        	<span>
	                            <a href=<?php echo $button_link; ?> class="b-buttons-group__button b-buttons-group__button_<?php echo get_row_index(); ?> b-side-text__button b-side-text__button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a> 
	                            </span>
	                        <?php else : ?>
	                        	<a href=<?php echo $button_link; ?> class="b-funneling__buttons__button <?php echo $grid; ?>" <?php echo $button_style; ?>><?php echo $button_label; ?></a>
	                        <?php endif;  // End check for single button label and link ?>
	                        
	                    <?php endwhile; // End buttons loop?>
	    			</div>
    			<?php endif; // End check for button repeater ?>
		</div>
	</div></div>
</section>