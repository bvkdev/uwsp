<?php // CURRENT ROW LAYOUT = quote-carousel  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = ro_inverse_text();
?>

<?php
// Get number of slides
$slide_count = count(get_sub_field('quotes'));
if( $slide_count > 1 ) {
    $carousel_class = "js-carousel_quote";
}
?>
<section <?php echo $id_tag; ?> class="b-section  b-section_quotes b-section_outer-pad <?php echo $collapse_class; ?>  <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_quotes cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_quotes cf " >
		<?php if( have_rows('quotes') ): ?>
			<div class="b-quotes-carousel <?php echo $carousel_class; ?>">
		<?php while( have_rows('quotes') ): the_row(); ?>
			<?php
			$quote = get_sub_field('quote');
			$attribution = get_sub_field('attribution_1');
			$attribution_2 = get_sub_field('attribution_2');
			$attribution_3 = get_sub_field('attribution_3');	
			?>
		<div class="b-quote js-equal-heights_row slide">
			<div class="b-quote__words">
				<?php if($quote): ?> <p class="b-quote__body"><?php echo $quote; ?></p> <?php endif; ?>
				<div class="b-quote__attributions">
					<div class="b-quote__attribution-text">
						<?php if($attribution): ?> <p class="b-quote__attribution"><?php echo $attribution; ?></p> <?php endif; ?>
						<?php if($attribution_2): ?> <p class="b-quote__attribution"><?php echo $attribution_2; ?></p> <?php endif; ?>
						<?php if($attribution_3): ?> <p class="b-quote__attribution"><?php echo $attribution_3; ?></p> <?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
			</div>
            
		<?php endif; ?>
	</div></div>
</section>