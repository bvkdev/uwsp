<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = ro_inverse_text();
// Get values for page intro
$intro_headline = get_sub_field('headline');
$intro_subhead = get_sub_field('subheadline');
$intro_text = get_sub_field('intro');
$is_h1 = get_sub_field('make_h1');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_intro <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_intro cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_intro cf">
			<?php if($intro_headline): ?>
				<?php if($is_h1): ?>
					<h1 class="h2 b-section_intro__headline b-seo-title"><?php echo $intro_headline; ?></h1>
				<?php else : ?>
					<h2 class="h2 b-section_intro__headline b-seo-title"><?php echo $intro_headline; ?></h2>
				<?php endif; ?>
			<?php endif; ?>
			<?php if($intro_subhead): ?> <h3 class="b-section__subtitle b-section_intro__subhead"><?php echo $intro_subhead; ?></h3><?php endif; ?>
            <?php if($intro_text): ?> <div class="b-wysiwyg b-section_intro__text"><?php echo $intro_text; ?></div><?php endif; ?>
            <?php if( have_rows('button') ) : ?>
                <div class="b-section_intro__buttons b-buttons-group">
                <?php while( have_rows('button') ): the_row(); ?>
                    <?php
                        $button_details = bbox_get_button_details();
                        $button_label = $button_details["label"];
                        $button_link = $button_details["link"];
                    ?>
                    <?php if($button_label && $button_link): ?>
                        <a href=<?php echo $button_link; ?> class="b-buttons-group__button b-buttons-group__button_<?php echo get_row_index(); ?> b-section_intro__button b-section_intro__button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a>
                    <?php endif;  // End check for single button label and link ?>
                    
                <?php endwhile; // End buttons loop?>
                </div>
            <?php endif; // End check for button repeater ?>
		</div>
	</div>
</section>