<?php // CURRENT ROW LAYOUT = side-image-text-area  ?>
<?php
// Get values via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$inverse_class = ro_inverse_text();
$collapse_class = ro_collapse_padding($ro_prefix);
//
$section_title = get_sub_field('section_title');
$button_label = ro_get_button_label();
$section_button_link = ro_button_link();
?>

<?php
$mobile_display_setting = get_sub_field('mobile_only');
if($mobile_display_setting) {
    $mobile_display_setting_class = "mobile_only";
} else {
    $mobile_display_setting_class = "";
}
$funnel_buttons = get_sub_field('funneling_button');

// Set up grid for quicklinks buttons
$grid_width = get_sub_field('button_grid_columns');
$grid = "";
if($grid_width == ""):
    $grid_width = 1;
    $grid = "p-all p-all t-all d-all";
elseif($grid_width == 1):
    $grid = "p-all p-all t-all d-all";
elseif($grid_width>1 && $grid_width <= 4):
    $grid = "p-1of2 m-1of2 t-1of".$grid_width." d-1of".$grid_width;
elseif($grid_width == 5 || $grid_width == 6):
    $grid = "p-1of2 m-1of2 t-1of".$grid_width." d-1of".$grid_width;
elseif($grid_width == 7 || $grid_width == 8):
    $grid = "p-1of2 m-1of4 t-1of4 d-1of".$grid_width;
endif; 

    // get the text values
    $headline = get_sub_field( 'headline' );


//
$is_h1 = get_sub_field('make_h1');
?>

<section <?php echo $id_tag; ?> class="b-section <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?> b-section_quicklinks <?php echo $mobile_display_setting_class; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_quicklinks">
		<div class="b-section__wrap-inner b-section__wrap-inner_quicklinks" >
		<?php if($headline): ?>
            <?php if($is_h1): ?>
                <h1 class="b-quicklinks__headline"><span><?php echo $headline; ?></span></h1>
            <?php else : ?>
                <h2 class="b-quicklinks__headline"><span><?php echo $headline; ?></span></h2>
            <?php endif; ?>
        <?php endif; ?>
		<div class="b-quicklinks__content">
			
		    <?php if( have_rows('funneling_button') ) : ?>
            <div class="b-quicklinks__buttons b-columns ">
                <?php while( have_rows('funneling_button') ): the_row(); ?>
                    <?php
                        $button_details = bbox_get_button_details();
                        $button_label = $button_details["label"];
                        $button_link = $button_details["link"];
                        $button_image = $button_details["image"];
                        $button_style = "style='background: url(".$button_image.") center center no-repeat rgba(73, 47, 146, 0.8); background-size: cover; background-blend-mode: soft-light;'";

                    ?>
                    <?php if($button_label && $button_link): ?>
                        <a href=<?php echo $button_link; ?> class="b-quicklinks__buttons__button <?php echo $grid; ?>" <?php echo $button_style; ?>><?php echo $button_label; ?></a>
                    <?php endif;  // End check for single button label and link ?>
                    
                <?php endwhile; // End buttons loop?>
                <?php
                $cards_amount = count($funnel_buttons);
                //echo "cards_amount is ".$cards_amount;
                //echo '<br />';
                //echo "row_amount is ".$row_amount;
                $last_row_spots = $cards_amount % $grid_width;
                //echo '<br />';
                //echo "last_row_spots is ".$last_row_spots;
                
                if ($last_row_spots != 0) {
                    $missing_spots = $grid_width - $last_row_spots;
                    //echo '<br />';
                    //echo "missing_spots is ".$missing_spots;
                    for ($i=0; $i < $missing_spots; $i++) { 
                        echo '<div class="b-quicklinks__buttons__button b-grid-fixer '.$grid.'"></div>';
                    }
                }
                ?>
            </div>
            <?php endif; // End check for quicklinks button repeater ?>            
	</div>
</div>
</section>