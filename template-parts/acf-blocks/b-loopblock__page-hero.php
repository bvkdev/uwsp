<?php // CURRENT ROW LAYOUT = page-header  ?>
<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = ro_inverse_text();
?>


<?php
//Has an image background?
$has_bkg_image = get_sub_field('background-image');


//SLIDE CONTENT
$slide_headline = get_sub_field('headline');
$slide_subheadline = get_sub_field('subheadline');
$slide_text = get_sub_field('intro');
//H1
$is_h1 = get_sub_field('make_h1');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_hero  <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?> >
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_hero">
		<div class="b-section__wrap-inner b-section__wrap-inner_hero">
				<div class="b-hero__content">
                <?php if($slide_headline != ""): ?>
					<?php if($is_h1): ?>
						<h1 class="h2 b-headline b-hero__headline"><?php echo $slide_headline; ?></h1>
					<?php else : ?>
						<h2 class="h2 b-headline b-hero__headline"><?php echo $slide_headline; ?></h2>
					<?php endif; ?>
				<?php endif; ?>
				<?php if($slide_subheadline != ""): ?><p class="b-subhead b-hero__subhead"><span><?php echo $slide_subheadline; ?></span></p><?php endif; ?>
				<?php if($slide_text != ""): ?><div class="b-wysiwyg b-hero__text"><?php echo $slide_text; ?></div><?php endif; ?>

                <?php if( have_rows('button') ) : ?>
                    <div class="b-hero__buttons b-buttons-group">
                    <?php while( have_rows('button') ): the_row(); ?>
                        <?php
                        $button_details = bbox_get_button_details();
                        $button_label = $button_details["label"];
                        $button_link = $button_details["link"];
                        ?>
                        <?php if($button_label && $button_link): ?>
                            <a href=<?php echo $button_link; ?> class="b-buttons-group__button b-buttons-group__button_<?php echo get_row_index(); ?> b-hero__button b-hero__button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a>
                        <?php endif;  // End check for single button label and link ?>
                        
                    <?php endwhile; // End buttons loop?>
                    </div>
                <?php endif; // End check for button repeater ?>
		</div>
        </div>
	</div>
</section>