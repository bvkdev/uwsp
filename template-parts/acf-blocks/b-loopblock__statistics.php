<?php
// Get values for page intro via common-functions
$ro_prefix = '';
$id_tag = ro_add_section_id($ro_prefix);
$custom_classes = ro_add_section_classes($ro_prefix);
$section_bkg_style = ro_set_background_style($ro_prefix);
$collapse_class = ro_collapse_padding($ro_prefix);
$inverse_class = ro_inverse_text();
// Get values for page intro
$intro_headline = get_sub_field('headline');
$is_h1 = get_sub_field('make_h1');
?>

<section <?php echo $id_tag; ?> class="b-section b-section_statistics <?php echo $collapse_class; ?> <?php echo $inverse_class; ?> <?php echo $custom_classes; ?>" <?php echo $section_bkg_style; ?>>
	<div class="wrap b-section__wrap-outer b-section__wrap-outer_statistics cf">
		<div class="b-section__wrap-inner b-section__wrap-inner_statistics cf">
			<?php if($intro_headline): ?>
				<?php if($is_h1): ?>
					<h1 class="h2 b-section_statistics__headline"><?php echo $intro_headline; ?></h1>
				<?php else : ?>
					<h2 class="h2 b-section_statistics__headline"><?php echo $intro_headline; ?></h2>
				<?php endif; ?>
			<?php endif; ?>
            <?php if( have_rows('button') ) : ?>
                <div class="b-section_statistics__buttons b-buttons-group">
                <?php while( have_rows('button') ): the_row(); ?>
                    <?php
                        $button_details = bbox_get_button_details();
                        $button_label = $button_details["label"];
                        $button_link = $button_details["link"];
                    ?>
                    <?php if($button_label && $button_link): ?>
                        <a href=<?php echo $button_link; ?> class="b-buttons-group__button  b-section_statistics__button b-section_statistics__button_<?php echo get_row_index(); ?>"><?php echo $button_label; ?></a>
                    <?php endif;  // End check for single button label and link ?>
                    
                <?php endwhile; // End buttons loop?>
                </div>
            <?php endif; // End check for button repeater ?>

            <?php if( have_rows('stat') ) : ?>
                <ul class="b-statistics">
                <?php while( have_rows('stat') ): the_row(); ?>
                    <?php
                        $stat_value = get_sub_field("value");
                        $stat_label = get_sub_field("label");
                    ?>
                    <li class="b-statistic">
                        <span class="b-statistic__value"><?php echo $stat_value; ?></span>
                        <span class="b-statistic__label"><?php echo $stat_label; ?></span>
                    </li>
                <?php endwhile; // End buttons loop?>
                </ul>
            <?php endif; // End check for button repeater ?>
		</div>
	</div>
</section>