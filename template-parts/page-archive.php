<div id="primary" class="uwsp-archive content-area">
    <main id="main" class="site-main b-columns t-all p-all">
        <?php get_sidebar('archive'); ?>
        <section class="d-2of3 t-all p-all">
            <?php if ( have_posts() ) : ?>
                <header class="page-header">
                    <h1 class="page-title"><?= single_cat_title('', false) ?></h1>
                    <?php getwid_base_posts_pagination(); ?>
                </header><!-- .entry-header -->
                <div class="archive-posts">
                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) :
                        the_post();
                        /*
                         * Include the Post-Type-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                         */
                        get_template_part( 'template-parts/content-loop', get_post_type() );
                    endwhile;
                    getwid_base_posts_pagination();
                    ?>
                </div>
            <?php
            else :
                get_template_part( 'template-parts/content', 'none' );
            endif;
            ?>
        </section>
    </main>
</div>
