<?php
/**
 * The template for displaying blogroll pages
 */
get_header();
$bkg       = get_the_post_thumbnail_url(get_queried_object_id(), 'large') ?? false;
?>

<?php if ( $bkg) : ?>
    <style>
    .news-events__feature::before {
        background-image: url(<?= $bkg ?>);
    }
    </style>
<?php endif; ?>

<?php if ( have_posts() ) : ?>
    <?php if ( is_paged() ) : ?>
        <?php get_template_part('template-parts/page-archive'); ?>
    <?php else : ?>
        <div id="primary" class="news-events content-area">
            <main id="main" class="site-main b-columns t-all p-all">
                <section class="d-all t-all p-all">
                    <div class="news-events__feature">
                        <?php
                        while( have_posts() ) : the_post();
                            get_template_part('template-parts/content-loop', get_post_type(), [
                                'button' => 'read story',
                                'eyebrow' => reset(wp_get_post_categories($post->ID, [
                                    'number' => 1,
                                    'fields' => 'names',
                                ]))
                            ]);
                            break;
                        endwhile;
                        ?>
                    </div>
                    <div class="news-events__secondary">
                        <?php
                        $count = 0;
                        while( have_posts() && $count < 3 ) : the_post();
                            get_template_part('template-parts/content-loop', get_post_type(), ['button' => 'read story']);
                            $count++;
                        endwhile;
                        ?>
                    </div>
                    <div class="uwsp-filter">
                        <ul>
                            <!-- <li><a href="#" tab-index="-1" aria-hidden="true">all</a></li> -->
                            <?php foreach ( get_categories(['parent' => 0]) as $cat ) : ?>
                                <li><a href="<?= get_category_link($cat) ?>"><?= $cat->name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="news-events__tertiary">
                        <?php
                        while( have_posts() ) : the_post();
                            get_template_part('template-parts/content-loop', get_post_type());
                        endwhile;
                        ?>
                    </div>
                    <button class="uwsp-load-more"><?= get_next_posts_link('view more<i class="far fa-arrow-alt-circle-right"></i>') ?></button>
                </section>
            </main>
        </div>
    <?php endif; ?>
<?php else : ?>
    <?php get_template_part( 'template-parts/content', 'none' ); ?>
<?php endif; ?>

<?php
get_footer();
